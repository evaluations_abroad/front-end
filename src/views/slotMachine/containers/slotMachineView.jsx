import React, { Component } from 'react';
import { connect } from 'react-redux';
import { compose, bindActionCreators } from 'redux';
import { withStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';
import { spinReels, insertCoins } from '../ducks/actions'
// import appleImage from '../../../images/apple.png';
import FruitImage from '../../../commons/FruitImage';
import fruitType from '../../../utils/fruitType';
import styles from '../style';
import RingLoader from "react-spinners/RingLoader";

const override = `
  display: block;
  margin: 0 auto;
  border-color: red;
`;
class SlotMachineView extends Component {

  constructor() {
    super();
    this.onSpin = this.onSpin.bind(this);
    this.onInsertCoins = this.onInsertCoins.bind(this);
  }

  componentWillMount() {
  }

  onSpin = () => {
    this.props.spinReels();
  }

  onInsertCoins = () => {
    this.props.insertCoins();
  }

  render() {
    let { classes, coins, result, loading } = this.props;

    return (
      <div>
        <div className={classes.appBarSpacer} />
        <div className={classes.headerContainer}>
          <Typography
            variant="h4"
            gutterBottom
            component="h2"
          >
            Slot machine
          </Typography>
        </div>
        <div className={classes.tableContainer}>
          <Card>
            <CardContent>
              <Grid container>
                <Grid container item xs={12}>
                  <Grid item container xs={9}>
                    {
                      result.spinResult.length > 0 ?
                        result.spinResult.map(r => (
                          <Grid item xs={3}>
                            <RingLoader
                              css={override}
                              size={150}
                              color={"#123abc"}
                              loading={loading}
                            />
                            <FruitImage type={r} visible={!loading} />
                          </Grid>
                        ))
                        :
                        [
                          <Grid item xs={3}>
                            <RingLoader
                              css={override}
                              size={150}
                              color={"#123abc"}
                              loading={loading}
                            />
                            <FruitImage type={fruitType.APPLE} visible={!loading} />
                          </Grid>,
                          <Grid item xs={3}>
                            <RingLoader
                              css={override}
                              size={150}
                              color={"#123abc"}
                              loading={loading}
                            />
                            <FruitImage type={fruitType.BANANA} visible={!loading} />
                          </Grid>,
                          <Grid item xs={3}>
                            <RingLoader
                              css={override}
                              size={150}
                              color={"#123abc"}
                              loading={loading}
                            />
                            <FruitImage type={fruitType.LEMON} visible={!loading} />
                          </Grid>
                        ]
                    }
                  </Grid>
                  <Grid container item xs={3}>
                    <Grid item xs={6}>
                      Current coins {coins}
                    </Grid>
                    <Grid item xs={6}>
                      <Button variant="contained" size="large" color="primary"
                        className={classes.margin} fullWidth
                        onClick={this.onSpin}
                        disabled={loading || coins === 0}>
                        Spin
                      </Button>
                      <Button variant="contained" size="large" color="primary"
                        className={classes.margin} fullWidth
                        onClick={this.onInsertCoins}
                        disabled={coins !== 0}>
                        Insert 20 coins
                      </Button>
                    </Grid>
                  </Grid>
                </Grid>
                <Grid item xs={12}>
                  <Typography
                    variant="h4"
                    gutterBottom
                    component="h2"
                  >
                    {!loading ?
                      (result.reward !== 0 ? `You WON ${result.reward} coins!!!` : `Good luck!`)
                      : ''}
                  </Typography>
                </Grid>
              </Grid>
            </CardContent>
          </Card>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  loading: state.slotMachine.loading,
  coins: state.slotMachine.coins,
  result: state.slotMachine.result
});
const mapDispatchToProps = dispatch =>
  bindActionCreators({ spinReels, insertCoins }, dispatch);
export default compose(
  withStyles(styles),
  connect(
    mapStateToProps,
    mapDispatchToProps
  )
)(SlotMachineView);