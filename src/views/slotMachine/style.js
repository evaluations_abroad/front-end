export default theme => ({
  appBarSpacer: theme.mixins.toolbar,
  content: {
    flexGrow: 1,
    padding: theme.spacing.unit * 3,
    height: '100vh',
    overflow: 'auto',
  },
  headerContainer: {
    width: '100%',
    display: 'flex',
    justifyContent: 'space-between'
  },
  tableContainer: {
    padding: '10px 0px',
    height: '300px'
  }
});
