import { INITIAL_VALUES } from './actions';
import types from './types';

export default (state = INITIAL_VALUES, action) => {
  switch (action.type) {
    case types.REEL_SPINNING_STARTED:
      return { ...state, coins: --state.coins, loading: true };
    case types.REEL_SPINNING_FINISHED:
      return { ...state, result: action.payload, loading: false };
    case types.REEL_INSERT_COIN_FINISHED:
      return { ...state, coins: 20 };
    default:
      return state;
  }
}