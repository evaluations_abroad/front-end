import types from './types';
import fruitType from '../../../utils/fruitType';

export const INITIAL_VALUES = {
    loading: false,
    coins: 20,
    reels: {
        reel1: [fruitType.CHERRY, fruitType.LEMON, fruitType.APPLE, fruitType.LEMON, fruitType.BANANA, fruitType.BANANA, fruitType.LEMON, fruitType.LEMON],
        reel2: [fruitType.LEMON, fruitType.APPLE, fruitType.LEMON, fruitType.LEMON, fruitType.CHERRY, fruitType.APPLE, fruitType.BANANA, fruitType.LEMON],
        reel3: [fruitType.LEMON, fruitType.APPLE, fruitType.LEMON, fruitType.APPLE, fruitType.CHERRY, fruitType.LEMON, fruitType.BANANA, fruitType.LEMON]
    },
    result: {
        spinResult: [],
        reward: 0
    }
};

export const spinReels = () => dispatch => {
    dispatch({ type: types.REEL_SPINNING_STARTED });

    const spinResult = [
        selectItem(INITIAL_VALUES.reels.reel1),
        selectItem(INITIAL_VALUES.reels.reel2),
        selectItem(INITIAL_VALUES.reels.reel3)
    ];

    setTimeout(() => {
        return dispatch({
            type: types.REEL_SPINNING_FINISHED,
            payload: {
                spinResult,
                reward: calculateReward(spinResult)
            }
        });
    }, 2000);
}

const selectItem = (array) => {
    return array[Math.floor(Math.random() * array.length)];
}

const calculateReward = spinResult => {
    const fruitsQuantities = countFruits(spinResult);

    if (fruitsQuantities.cherries === 3) return 50;
    if (fruitsQuantities.cherries === 3) return 50;
    if (fruitsQuantities.cherries === 2) return 40;
    if (fruitsQuantities.apples === 3) return 20;
    if (fruitsQuantities.apples === 2) return 10;
    if (fruitsQuantities.bananas === 3) return 15;
    if (fruitsQuantities.bananas === 2) return 5;
    if (fruitsQuantities.lemons === 3) return 3;
    return 0;
}

const countFruits = spinResult => {
    return {
        cherries: spinResult.filter(f => f === fruitType.CHERRY).length,
        apples: spinResult.filter(f => f === fruitType.APPLE).length,
        bananas: spinResult.filter(f => f === fruitType.BANANA).length,
        lemons: spinResult.filter(f => f === fruitType.LEMON).length
    }
}

export const insertCoins = () => dispatch => {
    return dispatch({
        type: types.REEL_INSERT_COIN_FINISHED
    });
}