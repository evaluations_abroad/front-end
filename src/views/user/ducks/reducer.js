import { INITIAL_VALUES } from './actions';
import types from './types';

export default (state = INITIAL_VALUES, action) => {
  switch (action.type) {
    case types.USER_FORM_RESET:
      return { ...state, user: INITIAL_VALUES.user, disabledForm: INITIAL_VALUES.disabledForm };
    case types.USER_LIST_FETCH_STARTED:
      return { ...state, loading: true };
    case types.USER_LIST_FETCH_FINISHED:
      return { ...state, users: action.payload, offset: action.offset, total: action.total, filters: action.filters, loading: false };
    case types.USER_CREATED:
      return { ...state };
    case types.USER_VIEW:
      return { ...state, disabledForm: true }
    case types.USER_FETCHED:
      return { ...state, user: action.payload, disabledForm: INITIAL_VALUES.disabledForm };
    case types.USER_UPDATED:
      return { ...state };
    default:
      return state;
  }
}