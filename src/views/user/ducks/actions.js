import ApiAgent, { HTTP_METHOD } from '../../../main/apiAgent'
import resources from '../../../main/resources';
import types from './types';
import pigeon from '../../../utils/pigeon';

export const INITIAL_VALUES = {
  users: [],
  user: {
    id: null,
    name: '',
    email: '',
    password: '',
    passwordConfirm: '',
    phone: '',
    immutable: false,
  },
  limit: 10,
  offset: 0,
  total: 0,
  filters: {},
  disabledForm: false,
  loading: false
};

export const formReset = () => dispatch => {
  dispatch({ type: types.USER_FORM_RESET });
}

export const list = async (limit, offset, whereFilters) => dispatch => {
  dispatch({ type: types.USER_LIST_FETCH_STARTED });
  limit = limit || INITIAL_VALUES.limit;
  offset = offset || INITIAL_VALUES.offset;

  return dispatch(ApiAgent.getList(
    `${resources.USER_RESOURCE}/all`,
    types.USER_LIST_FETCH_FINISHED));
}

export const get = async (id, dispatchType) => dispatch => {
  ApiAgent.submit(
    resources.USER_RESOURCE,
    HTTP_METHOD.GET,
    { params: { id } }
  ).then((response) => {
    dispatch({
      type: types.USER_FETCHED,
      payload: response.data
    });
    if (dispatchType) {
      dispatch({ type: dispatchType });
    }
  })
}

export const create = async (values) => dispatch => {
  ApiAgent.submit(
    resources.USER_RESOURCE,
    HTTP_METHOD.POST,
    { params: values }
  ).then((response) => {
    if (response && response.status === 200) {
      dispatch({ type: types.USER_CREATED });
      dispatch(formReset());
      dispatch(pigeon('success'));
    } else {
      dispatch(pigeon('error'));
    }
  })
}