import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators, compose } from 'redux';

import * as Yup from 'yup';
import { withStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';

// import messages from '../../../utils/messages';
import CustomInput from '../../../commons/fields/CustomInput';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import FormComponent from '../../../commons/FormComponent';
import { formReset, create } from '../ducks/actions'
import resources from '../../../main/resources';

const styles = theme => ({
  appBarSpacer: theme.mixins.toolbar,
  content: {
    flexGrow: 1,
    padding: theme.spacing.unit * 3,
    height: '100vh',
    overflow: 'auto'
  },
  tableContainer: {
    height: 320
  },
  h5: {
    marginBottom: theme.spacing.unit * 2
  },
  card: {
    marginBottom: 20
  }
});
class UserForm extends Component {

  goToRoute = (route) => {
    const { formReset, history } = this.props;
    formReset()
    history.push(`/${route}`)
  };

  handleSubmit = (values) => {
    const { create, update } = this.props;
    if (values.id) {
      values.password = "nnn";
      update(values).then(() => { this.goToRoute(resources.USERS_RESOURCE) });
    } else {
      create(values).then(() => { this.goToRoute(resources.USERS_RESOURCE) });
    }
  };

  renderPasswordFields(user, disabledForm) {
    if (user.id) {
      return <div />
    } else {
      return [
        <CustomInput type="password" name='password' label='Password' sm={3} disabled={disabledForm} />,
        <CustomInput type="password" name='passwordConfirm' label='Confirm password' sm={3} disabled={disabledForm} />
      ]
    }
  }

  render() {
    let { classes, user, disabledForm } = this.props;

    const schema = Yup.object().shape({
      name: Yup.string().required(),
      email: Yup.string().required().email(),
      password: Yup.string()
        .when('id', {
          is: val => val == null,
          then: Yup.string().required()
        }),
      passwordConfirm: Yup.string()
        .when('id', {
          is: val => val == null,
          then: Yup.string()
            .required("confirm password is a required field")
            .test(
              "match",
              "the confirmation does not match password",
              function (pwdConfirm) {
                return pwdConfirm === this.parent.password
              }
            )
        })
    });

    const buttons = [];
    if (!disabledForm) {
      buttons.push({
        label: (user.id ? 'Update' : 'Save'),
        type: 'submit',
        fullWidth: false,
        variant: 'contained',
        color: 'secondary'
      });
    }
    buttons.push({
      label: 'Back',
      fullWidth: false,
      variant: 'contained',
      color: 'default',
      onClick: () => this.goToRoute(resources.OPERATORS_RESOURCE)
    });

    const entityLabel = "user";

    return (
      <div>
        <div className={classes.appBarSpacer} />
        <Typography variant="h4" gutterBottom component="h2">
          {(disabledForm ? `View ${entityLabel}` : (user.id ? `Edit ${entityLabel}` : `Create ${entityLabel}`))}
        </Typography>
        <div className={classes.tableContainer}>
          <Card>
            <CardContent>
              <FormComponent
                handleSubmit={this.handleSubmit}
                validationSchema={schema}
                initialValues={user}
                enableReinitialize={true}
                buttons={buttons}
              >
                <CustomInput name='name' label='Name' sm={3} disabled={disabledForm} />
                <CustomInput name='email' label='Email' sm={3} disabled={disabledForm} />
                {this.renderPasswordFields(user, disabledForm)}
              </FormComponent>
            </CardContent>
          </Card>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  user: state.user.user,
  disabledForm: state.user.disabledForm
});
const mapDispatchToProps = dispatch =>
  bindActionCreators(
    { formReset, create },
    dispatch
  );
export default compose(
  withStyles(styles),
  connect(
    mapStateToProps,
    mapDispatchToProps
  )
)(UserForm);