import React, { Component } from 'react';
import { connect } from 'react-redux';
import { compose, bindActionCreators } from 'redux';
import { withStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton';
import Edit from '@material-ui/icons/Edit';
import Delete from '@material-ui/icons/Delete';
import Assignment from '@material-ui/icons/Assignment';
import Fab from '@material-ui/core/Fab';
import AddIcon from '@material-ui/icons/Add';
import Tooltip from '@material-ui/core/Tooltip';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';

import DataGrid from '../../../commons/DataGrid';
import { get, list } from '../ducks/actions'
import resources from '../../../main/resources';
import { confirmAlert } from 'react-confirm-alert';
import 'react-confirm-alert/src/react-confirm-alert.css';
import types from '../ducks/types'

const styles = theme => ({
  appBarSpacer: theme.mixins.toolbar,
  content: {
    flexGrow: 1,
    padding: theme.spacing.unit * 3,
    height: '100vh',
    overflow: 'auto',
  },
  tableContainer: {
    height: 320,
    padding: '10px 0px'
  },
  headerContainer: {
    width: '100%',
    display: 'flex',
    justifyContent: 'space-between'
  },
  fabAdd: {
    position: 'absolute',
    bottom: theme.spacing.unit * 2,
    right: theme.spacing.unit * 2,
  },
  fabAdd2: {
    position: 'absolute',
    bottom: theme.spacing.unit * 10,
    right: theme.spacing.unit * 2.5,
  },
  iconButton: {
    padding: '5px'
  },
  cardSearchForm: {
    margin: '10px 0 10px 0'
  },
  buttonMargin: {
    margin: theme.spacing.unit * 2,
  }
});

class UserList extends Component {

  create = () => {
    const { history } = this.props;
    history.push(resources.USER_RESOURCE)
  };

  edit = (selected) => {
    const { get, history } = this.props;
    get(selected.id).then(() => {
      history.push(resources.USER_RESOURCE)
    });
  };

  show = (selected) => {
    const { get, history } = this.props;
    get(selected.id, types.USER_VIEW).then(() => {
      history.push(resources.USER_RESOURCE)
    });
  };

  remove = (selected) => {
    const { remove, list } = this.props;
    confirmAlert({
      title: 'Excluir',
      message: "confirma a exclusão?",
      buttons: [
        {
          label: 'OK',
          onClick: () => remove(selected).then(() => list())
        },
        {
          label: 'Cancelar'
        }
      ],
    });
  };

  componentWillMount() {
    const { list } = this.props;
    list();
  }

  loadGridData = (offset, filters) => {
    const { list, limit } = this.props;
    list(limit, offset, filters);
  }

  onPage = async (event) => {
    const { list, limit } = this.props;
    list(limit, event.first);
  }

  actionTemplate = (rowData, column) => {
    const { classes } = this.props;
    return (
      <div>
        <Tooltip title="View" placement='bottom'>
          <IconButton color="default" className={classes.iconButton} onClick={() => this.show(rowData)}>
            <Assignment fontSize="small" />
          </IconButton>
        </Tooltip>
        <Tooltip title="Edit" placement='bottom'>
          <IconButton color="default" className={classes.iconButton} onClick={() => this.edit(rowData)}>
            <Edit fontSize="small" />
          </IconButton>
        </Tooltip>
        <Tooltip title="Remove" placement='bottom'>
          <IconButton color="default" className={classes.iconButton} onClick={() => this.remove(rowData)}>
            <Delete fontSize="small" />
          </IconButton>
        </Tooltip>
      </div>
    );
  }

  render() {
    let { classes, users, total = users.length, loading } = this.props;

    const cols = [
      { field: 'name', header: 'Name', filter: false },
      { field: 'email', header: 'Email', filter: false },
      // {
      //   body: (rowData, column) => !rowData.immutable ? this.actionTemplate(rowData, column) : '',
      //   props: { style: { textAlign: 'center', width: '10%' } }
      // }
    ];

    return (
      <div>
        <div className={classes.appBarSpacer} />
        <div className={classes.headerContainer}>
          <Typography
            variant="h4"
            gutterBottom
            component="h2"
          >
            Users
          </Typography>
        </div>
        <div className={classes.tableContainer}>
          <Card>
            <CardContent>
              <DataGrid
                rows={0}
                paginator={false}
                header=''
                cols={cols}
                onChange={this.loadGridData}
                data={users}
                total={total}
                loading={loading}
              />
            </CardContent>
            <Tooltip title="New" aria-label="">
              <Fab className={classes.fabAdd} size='medium' color='secondary' onClick={this.create}>
                <AddIcon />
              </Fab>
            </Tooltip>
          </Card>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  users: state.user.users,
  loading: state.user.loading
});
const mapDispatchToProps = dispatch =>
  bindActionCreators({ get, list }, dispatch);
export default compose(
  withStyles(styles),
  connect(
    mapStateToProps,
    mapDispatchToProps
  )
)(UserList);