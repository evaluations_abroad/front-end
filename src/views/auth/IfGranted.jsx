import { Component } from 'react';
import { connect } from 'react-redux';

class IfGranted extends Component {
  render() {
    const { allowedRoles, auth } = this.props;
    if (auth.user.roles.some((val) => allowedRoles.indexOf(val) !== -1)) {
      return this.props.children;
    } else {
      return null;
    }
  }
}

const mapStateToProps = state => ({ auth: state.auth });
export default connect(
  mapStateToProps,
  null
)(IfGranted);
