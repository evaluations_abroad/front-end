import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

export const Authorization = allowedRoles => WrappedComponent => {
  class WithAuthorization extends Component {
    render() {
      const { user } = this.props.auth;
      if (user.roles.some((val) => allowedRoles.indexOf(val) !== -1)) {
        return <WrappedComponent {...this.props} />;
      } else {
        return <h2 style={{ paddingTop: 60, color: '#a22424' }}>Voce não possui permissão para acessar este recurso!</h2>;
      }
    }
  }

  const mapStateToProps = state => ({ auth: state.auth });
  const mapDispatchToProps = dispatch =>
    bindActionCreators({}, dispatch);
  return connect(
    mapStateToProps,
    mapDispatchToProps
  )(WithAuthorization);
};
