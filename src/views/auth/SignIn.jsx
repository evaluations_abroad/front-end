import React, { Component, Fragment } from 'react';
import { compose, bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { withRouter } from "react-router";

import Typography from '@material-ui/core/Typography';
import withStyles from '@material-ui/core/styles/withStyles';

import { doSignIn } from '../../state/auth/authActions';
import FormWithFormik from '../../commons/FormWithFormik';
import Yup from '../../utils/yup-customs';

const styles = theme => ({
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing.unit
  },
  link: {
    textDecoration: 'none',
    paddingTop: 12
  }
});

class SignIn extends Component {
  handleSubmit = async values => {
    const user = await this.props.doSignIn(values);
    if (user) {
      this.props.history.push('/');
    }
  };

  sendToSignUp = () => {
    this.props.history.push('/signup');
  };

  sendToRecoverAccess = () => {
    this.props.history.push('/forgotPassword');
  };

  render() {
    const { classes } = this.props;

    const schema = Yup.object().shape({
      email: Yup.string()
        .email()
        .required(),
      password: Yup.string().required()
    });

    return (
      <Fragment>
        <Typography component="h1" variant="h5" color="primary">
          Login
        </Typography>
        <div className={classes.form}>
          user: admin@mail.com /
          password: 123456
          <FormWithFormik
            initialValues={{
              email: (this.props.location.state || { email: '' }).email,
              password: ''
            }}
            fields={[
              {
                name: 'email',
                label: 'Email',
                autoComplete: 'off',
                variant: "outlined",
                margin: "dense"
              },
              {
                name: 'password',
                label: 'Password',
                autoComplete: 'off',
                type: 'password',
                variant: "outlined",
                margin: "dense"
              }
            ]}
            validationSchema={schema}
            buttons={[
              {
                label: 'Submit',
                type: 'submit',
                fullWidth: true,
                variant: 'contained',
                color: 'secondary'
              }
            ]}
            handleSubmit={this.handleSubmit}
          />
        </div>
      </Fragment>
    );
  }
}

const mapStateToProps = state => ({});
const mapDispatchToProps = dispatch =>
  bindActionCreators({ doSignIn }, dispatch);
export default compose(
  withRouter,
  withStyles(styles),
  connect(
    mapStateToProps,
    mapDispatchToProps
  )
)(SignIn);