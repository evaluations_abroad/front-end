import React, { Component } from 'react';
import { connect } from 'react-redux';
import { compose, bindActionCreators } from 'redux';
import { withStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';

import DataGrid from '../../../commons/DataGrid';
import { list } from '../ducks/actions'
import 'react-confirm-alert/src/react-confirm-alert.css';

const styles = theme => ({
  appBarSpacer: theme.mixins.toolbar,
  content: {
    flexGrow: 1,
    padding: theme.spacing.unit * 3,
    height: '100vh',
    overflow: 'auto',
  },
  tableContainer: {
    height: 320,
    padding: '10px 0px'
  },
  headerContainer: {
    width: '100%',
    display: 'flex',
    justifyContent: 'space-between'
  },
  iconButton: {
    padding: '5px'
  },
  cardSearchForm: {
    margin: '10px 0 10px 0'
  },
  buttonMargin: {
    margin: theme.spacing.unit * 2,
  }
});

class CountryList extends Component {

  componentWillMount() {
    const { list } = this.props;
    list();
  }

  loadGridData = (offset, filters) => {
    const { list } = this.props;
    list(filters);
  }

  render() {
    let { classes, countries, total = countries.length, loading } = this.props;

    const cols = [
      { field: 'name', header: 'Name', filter: true }
    ];

    return (
      <div>
        <div className={classes.appBarSpacer} />
        <div className={classes.headerContainer}>
          <Typography
            variant="h4"
            gutterBottom
            component="h2"
          >
            Countries
          </Typography>
        </div>
        <div className={classes.tableContainer}>
          <Card>
            <CardContent>
              <DataGrid
                rows={0}
                paginator={false}
                header=''
                cols={cols}
                onChange={this.loadGridData}
                data={countries}
                total={total}
                loading={loading}
              />
            </CardContent>
          </Card>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  countries: state.country.countries,
  loading: state.country.loading
});
const mapDispatchToProps = dispatch =>
  bindActionCreators({ list }, dispatch);
export default compose(
  withStyles(styles),
  connect(
    mapStateToProps,
    mapDispatchToProps
  )
)(CountryList);