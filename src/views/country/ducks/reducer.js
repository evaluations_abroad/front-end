import { INITIAL_VALUES } from './actions';
import types from './types';

export default (state = INITIAL_VALUES, action) => {
  switch (action.type) {
    case types.COUNTRY_LIST_FETCH_STARTED:
      return { ...state, loading: true };
    case types.COUNTRY_LIST_FETCH_FINISHED:
      return { ...state, countries: action.payload, filters: action.filters, loading: false };
    default:
      return state;
  }
}