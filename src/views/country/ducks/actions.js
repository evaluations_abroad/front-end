import ApiAgent, { HTTP_METHOD } from '../../../main/apiAgent'
import types from './types';
import resources from '../../../main/resources';

export const INITIAL_VALUES = {
  countries: [],
  filters: {},
  loading: false
};

export const list = async ({ name } = {}) => dispatch => {
  dispatch({ type: types.COUNTRY_LIST_FETCH_STARTED });
  if (name) {
    ApiAgent.submit(
      `${resources.COUNTRY_RESOURCE}/consult`,
      HTTP_METHOD.POST,
      { params: { name } }
    ).then((response) => {
      dispatch({
        type: types.COUNTRY_LIST_FETCH_FINISHED,
        payload: response.data.data
      });
    })
  } else {
    dispatch({ type: types.COUNTRY_LIST_FETCH_STARTED });
    return dispatch(ApiAgent.getList(
      `${resources.COUNTRY_RESOURCE}/all`,
      types.COUNTRY_LIST_FETCH_FINISHED));
  }
}