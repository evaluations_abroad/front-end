const COUNTRY_LIST_FETCH_STARTED = 'views/country/COUNTRY_LIST_FETCH_STARTED';
const COUNTRY_LIST_FETCH_FINISHED = 'views/country/COUNTRY_LIST_FETCH_FINISHED';

export default {
  COUNTRY_LIST_FETCH_STARTED,
  COUNTRY_LIST_FETCH_FINISHED
};