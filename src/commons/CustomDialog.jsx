import React from 'react';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';

export default class CustomDialog extends React.Component {
  
  state = {
    open: this.props.open || false
  };

  componentWillReceiveProps(nextProps) {
    this.setState({ open: nextProps.open });
  }

  handleClose = () => {
    this.setState({ open: this.props.handleClose ? this.props.handleClose() : false });
  };

  render() {

    const { open } = this.state;

    const {
      title,
      description,
      buttons
    } = this.props;

    return (
      <div>
        <Dialog
          open={open||false}
          onClose={this.handleClose}
          aria-labelledby="alert-dialog-title"
          aria-describedby="alert-dialog-description"
        >
          <DialogTitle id="alert-dialog-title">
            {title}
          </DialogTitle>
          <DialogContent>
            <DialogContentText id="alert-dialog-description">
              {description}
            </DialogContentText>
          </DialogContent>
          <DialogActions>
            {buttons.map((button, key) => {
              return (
                <Button key={key} {...button}>
                  {button.label}
                </Button>
              );
            })}
          </DialogActions>
        </Dialog>
      </div>
    );
  }
}
