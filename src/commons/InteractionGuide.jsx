import React from 'react';
import { compose } from 'redux';

import { withStyles } from '@material-ui/core/styles';
import InfoIcon from '@material-ui/icons/Info';
import Typography from '@material-ui/core/Typography';

const styles = () => ({
  alert: {
    position: 'relative',
    padding: '.3rem 1.25rem',
    marginBottom: '1rem',
    border: '1px solid transparent',
    borderRadius: '.25rem'
  },
  alertWarning: {
    color: '#856404',
    backgroundColor: '#fff3cd',
    borderColor: '#ffeeba'
  },
  alertIcon: {
    margin: '6px'
  },
  alertLabel: {
    position: 'absolute',
    display: 'inline-block',
    margin: '6px'
  }
});

const InteractionGuide = props => {
  const { label, classes: { alert, alertWarning, alertIcon, alertLabel } } = props;

  return (
    <div className={`${alert} ${alertWarning}`} role="alert">
      <InfoIcon className={alertIcon} />
      <Typography
        variant='subtitle1'
        className={alertLabel}
      >
        {label}
      </Typography>
    </div>
  );
};

export default compose(
  withStyles(styles)
)(InteractionGuide);
