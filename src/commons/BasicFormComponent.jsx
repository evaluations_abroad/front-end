import React from 'react';

import PropTypes from 'prop-types';
import { Form, withFormik } from 'formik';

import Grid from '@material-ui/core/Grid';
import withStyles from '@material-ui/core/styles/withStyles';

const styles = theme => ({
  errorMessage: {
    color: '#de5f24',
    paddingTop: 5,
    fontFamily: '"Goudy Bookletter 1911", sans-serif',
    fontSize: 12
  },
  button: {
    marginTop: theme.spacing.unit * 3
  }
});

const BasicFormComponent = withFormik({
  mapPropsToValues: props => props.initialValues,
  validationSchema: props => props.validationSchema,
  handleSubmit: (values, { props, resetForm, setErrors, setSubmitting }) => {
    props.handleSubmit(values);
  }
});

const CustomForm = props => {
  const children = React.Children.map(props.children, child => {
    return React.cloneElement(child, {
      highLevelProps: props
    });
  });

  return (
    <Form>
      <Grid container spacing={24}>
        {children}
      </Grid>
    </Form>
  );
};

BasicFormComponent.propTypes = {
  initialValues: PropTypes.object.isRequired,
  fields: PropTypes.array.isRequired,
  validationSchema: PropTypes.object,
  buttons: PropTypes.array.isRequired,
  handleSubmit: PropTypes.func.isRequired
};

export default BasicFormComponent(withStyles(styles)(CustomForm));
