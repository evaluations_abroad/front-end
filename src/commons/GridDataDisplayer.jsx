import React from 'react';
import { compose } from 'redux';

import { withStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';

import { chunkanizeArray } from '../utils/array';

const styles = theme => ({
});

const GridRowDisplayer = props => {
  const {
    data,
    columnWidth,
    gridWidth,
    textAlign,
    justifyContent,
    columnSpacing,
    gridElementGenerator,
    rowClass
  } = props;

  return (
    <Grid
      container
      item
      className={rowClass}
      xs={gridWidth}
      spacing={columnSpacing}
      alignItems={textAlign}
      justify={justifyContent}
    >
    {
      data.map(it => gridElementGenerator(it, columnWidth))
    }
    </Grid>
  );
}

const GridDataDisplayer = props => {
  const {
    data,
    columns,
    textAlign = 'center',
    rowGridJustify,
    gridWidth = 12,
    columnSpacing = 2,
    gridElementGenerator,
    rowClass,
  } = props;

  const dataSplittedInRows = chunkanizeArray(data, columns);
  return (
    <Grid
      container
      justify={rowGridJustify}
    >
    {
      dataSplittedInRows.map((rowObjects, index) =>
        (
          <GridRowDisplayer key={index}
            data={rowObjects}
            columnWidth={(gridWidth / columns)}
            gridWidth={gridWidth}
            columnSpacing={columnSpacing}
            gridElementGenerator={gridElementGenerator}
            textAlign={textAlign}
            justifyContent={rowGridJustify}
            rowClass={rowClass}
          />
        )
      )
    }
    </Grid>
  );
}

export default compose(
  withStyles(styles)
)(GridDataDisplayer);