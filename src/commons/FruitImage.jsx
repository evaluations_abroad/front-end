import React, { Component } from 'react';
import { withStyles } from '@material-ui/core/styles';

const styles = theme => ({
});
class FruitImage extends Component {

  render() {
    const { type, visible = true } = this.props;

    if (visible) {
      return (
        <div>
          <img src={`../images/fruits/${type.image}`} alt={type.name} />
        </div>
      );
    }
    return null;
  }
}

export default withStyles(styles)(FruitImage);