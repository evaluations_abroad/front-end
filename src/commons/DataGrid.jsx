import React, { Component } from 'react';
import PropTypes from 'prop-types';

import { withStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import MenuItem from '@material-ui/core/MenuItem';
import CircularProgress from '@material-ui/core/CircularProgress';
import SortNeutralIcon from '@material-ui/icons/UnfoldMore';
import SortAscIcon from '@material-ui/icons/ExpandLess';
import SortDescIcon from '@material-ui/icons/ExpandMore';
import { DataTable } from 'primereact/datatable';
import { Column } from 'primereact/column';
import { ContextMenu } from 'primereact/contextmenu';

const styles = theme => ({
  container: {
    display: 'flex',
    flexWrap: 'wrap',
  },
  textField: {
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit,
  },
  dense: {
    marginTop: 16,
  },
  menu: {
    width: 200,
  },
});

class DataGrid extends Component {
  constructor(props) {
    super(props);

    this.state = {
      first: 0,
      filters: {},
      sort: {},
      menu: this.props.menuItems
    };

    this.handleSortChange = this.handleSortChange.bind(this);
  }

  handleFiltersChange = (event, col) => {
    event.persist();
    let field = col.field;
    if (col.condition) field = `${col.field}_${col.condition}`;
    const value = event.target.value;
    let filters = { ...this.state.filters };
    filters[field] = value;
    if (!value) delete filters[field]
    this.setState({ filters: filters, first: 0 });
  }

  handleSortChange = (event, col) => {
    let field = col.field;

    let sort = { ...this.state.sort };

    if (sort[field]) {
      if (sort[field] === 'ASC') {
        sort[field] = 'DESC';
      }
      else {
        sort[field] = 'ASC';
      }
    } else {
      // SORT: Prisma does not support sort by multiple fields then the sort object is overrided by the next field.
      // When that feature is implemented remove the line below
      sort = {};

      sort[field] = 'ASC'
    }

    this.setState({ sort, first: 0 });
  }

  handleSelectChange = field => event => {
    this.setState({
      [field]: event.target.value,
    });
    this.handleFiltersChange(event, field);
  };

  shouldComponentUpdate(nextProps, nextState) {
    const { filters, sort } = this.state;
    if (filters !== nextState.filters || sort !== nextState.sort) {
      this.loadData(0, nextState.filters, nextState.sort);
    }
    return true;
  }

  handlePageChange = (event) => {
    const { filters, sort } = this.state;
    this.setState({ first: event.first });
    this.loadData(event.first, filters, sort);
  }

  handleContextMenuSelectionChange = event => {
    const { onContextMenuSelectionChange } = this.props;
    this.setState({ selected: event.value });
    if (onContextMenuSelectionChange) onContextMenuSelectionChange(event.value);
  }

  handleSelectionChange = event => {
    const { onSelectionChange } = this.props;
    this.setState({ selected: event.value });
    if (onSelectionChange) onSelectionChange(event);
  }

  loadData(page, filters, sort) {
    const { onChange } = this.props;

    const sortFields = Object.entries(sort).map(entry => `${entry[0]}_${entry[1]}`);

    // SORT: Prisma does not support sort by multiple fields.
    const orderBy = sortFields.length > 0 ? sortFields[0] : null;
    onChange(page, filters, orderBy);
  }

  renderSpinner() {
    if (this.props.fetching) {
      return (
        <div style={{ position: 'absolute', top: '92px' }}>
          <CircularProgress />
        </div>
      );
    }

    return null;
  }

  renderInput(col) {
    return (
      <div style={{ display: 'flex', justifyContent: 'space-between', alignItems: 'center' }}>
        <div style={{ flexGrow: 1 }}>
          {this.renderLabel(col)}
        </div>
        {this.renderSortButton(col)}
      </div>
    );
  }

  renderLabel(col) {
    if (!col.filter) {
      return <div>{col.header}</div>;
    }

    if (col.options) {
      const { field } = col;

      return (
        <TextField
          fullWidth
          select
          label={col.header}
          placeholder="Buscar..."
          value={this.state[field] || ''}
          onChange={this.handleSelectChange(field)}
          margin="none"
          style={{ paddingBottom: 10 }}
        >
          {col.options.map(({ value, label }) => (
            <MenuItem key={value} value={value}>
              {label}
            </MenuItem>
          ))}
        </TextField>
      );
    }

    if (col.date) {
      return (
        <TextField
          type='date'
          label={col.header}
          InputLabelProps={{ shrink: true }}
          style={{ display: 'flex' }}
          onChange={event => this.handleFiltersChange(event, col)}
        />
      );
    }

    return (
      <TextField
        label={col.header}
        placeholder="Buscar..."
        margin="none"
        style={{ paddingBottom: 10 }}
        onChange={event => this.handleFiltersChange(event, col)}
      />
    );
  }

  renderSortButton(col) {
    if (col.sort) {
      const sortState = this.state.sort[col.field];

      return (
        <div
          style={{
            display: 'flex', flexGrow: 0, justifyContent: 'center', alignItems: 'center',
            width: 16, height: 16, minWidth: 16, minHeight: 16, cursor: 'pointer'
          }}
          onClick={event => this.handleSortChange(event, col)}
        >
          {sortState ? (sortState === 'ASC' ? <SortAscIcon /> : <SortDescIcon />) : <SortNeutralIcon />}
        </div>
      );
    }

    return null;
  }

  render() {
    const { first, selected } = this.state;
    const { data, paginator = true, total, header, loading = false, menuItems, onSelectionChange,
      rows = 10,
      scrollable = false, scrollHeight,
      emptyMessage = 'Sem resultados' } = this.props;

    let features = {};

    if (menuItems) {
      features = {
        contextMenuSelection: selected,
        onContextMenuSelectionChange: this.handleContextMenuSelectionChange,
        onContextMenu: e => this.cm.show(e.originalEvent)
      };
    }

    if (onSelectionChange) {
      features = { ...features, selectionMode: 'single', selection: selected, onSelectionChange: this.handleSelectionChange }
    }

    return (
      <div style={{ position: 'relative', display: 'flex', justifyContent: 'center' }}>
        <DataTable
          responsive={true}
          loading={loading}
          paginator={paginator}
          value={data}
          totalRecords={total}
          rows={rows}
          first={first}
          header={header}
          lazy={true}
          onPage={this.handlePageChange}
          emptyMessage={emptyMessage}
          scrollable={scrollable}
          scrollHeight={scrollHeight}
          {...features}
        >
          {this.renderColumns()}
        </DataTable>
        <ContextMenu model={this.state.menu} ref={el => this.cm = el} onHide={() => this.setState({ selected: null })} />

        {this.renderSpinner()}
      </div>
    );
  }

  renderColumns() {
    const { cols } = this.props;
    return cols.map((col, i) => {
      let features = { header: '', filter: true, filterElement: this.renderInput(col) };

      if (col.body) {
        features = { ...features, body: col.body }
      }

      if (col.editor) {
        features = { ...features, editor: col.editor }
      }

      return (
        <Column
          key={`${i}_${col.field}`}
          field={col.field}
          {...features}
          {...col.props}
        />
      );

    });
  }
}

export default withStyles(styles)(DataGrid);

DataGrid.propTypes = {
  data: PropTypes.array.isRequired,
  cols: PropTypes.array.isRequired,
  paginator: PropTypes.bool,
  onChange: PropTypes.func.isRequired,
  total: PropTypes.number.isRequired,
  menuItems: PropTypes.array,
  onContextMenuSelectionChange: PropTypes.func,
  onSelectionChange: PropTypes.func
};
