import React, { Component } from 'react';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import ListSubheader from '@material-ui/core/ListSubheader';
import AssignmentIcon from '@material-ui/icons/Assignment';
import { withStyles } from '@material-ui/core/styles';

const styles = theme => ({
  root: {
    display: 'flex',
  },
  listItem: {
    paddingLeft: 24,
    [theme.breakpoints.down('xs')]: {
      paddingLeft: 16
    }
  }
});
class SecundaryListItems extends Component {
  render() {
    const { classes } = this.props;
    return (
      <div>
      <ListSubheader inset>Saved reports</ListSubheader>
      <ListItem className={classes.listItem} button>
        <ListItemIcon>
          <AssignmentIcon />
        </ListItemIcon>
        <ListItemText primary="Current month" />
      </ListItem>
      <ListItem className={classes.listItem} button>
        <ListItemIcon>
          <AssignmentIcon />
        </ListItemIcon>
        <ListItemText primary="Last quarter" />
      </ListItem>
      <ListItem className={classes.listItem} button>
        <ListItemIcon>
          <AssignmentIcon />
        </ListItemIcon>
        <ListItemText primary="Year-end sale" />
      </ListItem>
    </div>
    );
  }
}

export default withStyles(styles)(SecundaryListItems);