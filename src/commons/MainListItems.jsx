import React, { Component } from 'react';
import ListItem from '@material-ui/core/ListItem';
import { compose } from 'redux';
import { withRouter } from "react-router";

import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import PeopleIcon from '@material-ui/icons/People';
import LanguageIcon from '@material-ui/icons/Language';
import CasinoIcon from '@material-ui/icons/Casino';
import Tooltip from '@material-ui/core/Tooltip';
import { withStyles } from '@material-ui/core/styles';
import resources from '../main/resources';

const styles = theme => ({
  root: {
    display: 'flex',
  },
  listItem: {
    paddingLeft: 24,
    [theme.breakpoints.down('xs')]: {
      paddingLeft: 16
    }
  }, customTooltip: {
    fontSize: 18
  }
});

class MainListItems extends Component {

  goToRoute = (route) => {
    this.props.history.push(`/${route}`)
  };

  render() {
    const { classes } = this.props;
    return (
      <div>
        <Tooltip
          classes={{ tooltip: classes.customTooltip }}
          title="Users"
          placement="right">
          <ListItem className={classes.listItem} button onClick={() => this.goToRoute(resources.USERS_RESOURCE)}>
            <ListItemIcon>
              <PeopleIcon />
            </ListItemIcon>
            <ListItemText primary="Users" />
          </ListItem>
        </Tooltip>
        <Tooltip
          classes={{ tooltip: classes.customTooltip }}
          title="Countries"
          placement="right">
          <ListItem className={classes.listItem} button onClick={() => this.goToRoute(resources.COUNTRIES_RESOURCE)}>
            <ListItemIcon>
              <LanguageIcon />
            </ListItemIcon>
            <ListItemText primary="Countries" />
          </ListItem>
        </Tooltip>
        <Tooltip
          classes={{ tooltip: classes.customTooltip }}
          title="Slot Machine"
          placement="right">
          <ListItem className={classes.listItem} button onClick={() => this.goToRoute(resources.SLOT_MACHINE_RESOURCE)}>
            <ListItemIcon>
              <CasinoIcon />
            </ListItemIcon>
            <ListItemText primary="Slot Machine" />
          </ListItem>
        </Tooltip>
      </div>
    );
  }
}

export default compose(withRouter, withStyles(styles))(MainListItems);
