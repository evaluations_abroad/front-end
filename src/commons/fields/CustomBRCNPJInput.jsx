import React from 'react';

import PropTypes from 'prop-types';
import { Field, ErrorMessage } from 'formik';
import MaskedInput from 'react-text-mask';

import Grid from '@material-ui/core/Grid';
import FormControl from '@material-ui/core/FormControl';
import TextField from '@material-ui/core/TextField';
import withStyles from '@material-ui/core/styles/withStyles';
import CustomErrorMessageComponent from './CustomErrorMessageComponent';
import { errorMessageClass, inputClass } from '../layout/theme';

const cnpj_mask = [/\d/, /\d/, '.', /\d/, /\d/, /\d/, '.', /\d/, /\d/, /\d/, '/', /\d/, /\d/, /\d/, /\d/, '-', /\d/, /\d/];

const styles = theme => ({
  errorMessage: errorMessageClass,
  root: inputClass.root,
  item: inputClass.gridItem,
});
class CustomBRCNPJInput extends React.Component {

  /**
   * Usage:
   * https://github.com/text-mask/text-mask
   */

  TextMaskCustom = (props) => {
    const { inputRef, ...other } = props;
    return (
      <MaskedInput
        {...other}
        ref={ref => {
          inputRef(ref ? ref.inputElement : null);
        }}
        mask={cnpj_mask}
        placeholderChar={'\u2000'}
        showMask={false}
      />
    );
  };

  validate = value => {
    let errorMessage;
    if (value === undefined || value === "") {
      return errorMessage;
    }
    if (!this.validateCnpj(value)) {
      errorMessage = 'Número de CNPJ inválido';
    }
    return errorMessage;
  };

  validateCnpj = cnpj => {
    cnpj = cnpj.replace(/\D/g, "");
    let numbers, digits, sum, i, result, pos, length, equalsDigits;
    equalsDigits = 1;
    if (cnpj.length < 14 && cnpj.length < 15) {
      return false;
    }
    for (i = 0; i < cnpj.length - 1; i++) {
      if (cnpj.charAt(i) !== cnpj.charAt(i + 1)) {
        equalsDigits = 0;
        break;
      }
    }
    if (!equalsDigits) {
      length = cnpj.length - 2
      numbers = cnpj.substring(0, length);
      digits = cnpj.substring(length);
      sum = 0;
      pos = length - 7;
      for (i = length; i >= 1; i--) {
        sum += numbers.charAt(length - i) * pos--;
        if (pos < 2) {
          pos = 9;
        }
      }
      result = sum % 11 < 2 ? 0 : 11 - sum % 11;
      if (result !== Number(digits.charAt(0))) {
        return false;
      }
      length = length + 1;
      numbers = cnpj.substring(0, length);
      sum = 0;
      pos = length - 7;
      for (i = length; i >= 1; i--) {
        sum += numbers.charAt(length - i) * pos--;
        if (pos < 2)
          pos = 9;
      }
      result = sum % 11 < 2 ? 0 : 11 - sum % 11;
      if (result !== Number(digits.charAt(1))) {
        return false;
      }
      return true;
    } else {
      return false;
    }
  }

  CustomInputComponent = ({ field, ...props }) => {
    const { disabled, label } = props;
    return <TextField {...field} disabled={disabled}
      autoComplete='off' variant="outlined" margin="dense" label={label}
      InputProps={{
        inputComponent: this.TextMaskCustom,
      }} />
  };

  render() {
    const { classes, name, label, disabled = false, xs = 12, sm = 6, margin = 'normal' } = this.props;
    return (
      <Grid item xs={xs} sm={sm} className={classes.item}>
        <FormControl margin={margin} fullWidth className={classes.root}>
          <Field name={name} disabled={disabled} component={this.CustomInputComponent}
            autoComplete='off' validate={this.validate} label={label} />
          <ErrorMessage
            name={name}
            className={classes.errorMessage}
            component={CustomErrorMessageComponent}
          />
        </FormControl>
      </Grid>
    );
  }
}

CustomBRCNPJInput.propTypes = {
  name: PropTypes.string.isRequired,
  label: PropTypes.string.isRequired,
};

export default withStyles(styles)(CustomBRCNPJInput);