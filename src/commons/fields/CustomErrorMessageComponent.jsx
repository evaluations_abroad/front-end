import React from 'react';

export default ({ ...props }) => {
  if (typeof props.children === "string") {
    return <span className={props.className}>{props.children}</span>
  } else {
    console.log("props.children", props.children);
    return ""
  }
};
