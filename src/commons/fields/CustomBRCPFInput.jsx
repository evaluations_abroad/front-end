import React from 'react';

import PropTypes from 'prop-types';
import { Field, ErrorMessage } from 'formik';
import MaskedInput from 'react-text-mask';

import Grid from '@material-ui/core/Grid';
import FormControl from '@material-ui/core/FormControl';
import TextField from '@material-ui/core/TextField';
import withStyles from '@material-ui/core/styles/withStyles';
import CustomErrorMessageComponent from './CustomErrorMessageComponent';
import { errorMessageClass, inputClass } from '../layout/theme';

const cpf_mask = [/\d/, /\d/, /\d/, '.', /\d/, /\d/, /\d/, '.', /\d/, /\d/, /\d/, '-', /\d/, /\d/];

const styles = theme => ({
  errorMessage: errorMessageClass,
  root: inputClass.root,
  item: inputClass.gridItem,
});
class CustomBRCPFInput extends React.Component {

  /**
   * Usage:
   * https://github.com/text-mask/text-mask
   */

  TextMaskCustom = (props) => {
    const { inputRef, ...other } = props;
    return (
      <MaskedInput
        {...other}
        ref={ref => {
          inputRef(ref ? ref.inputElement : null);
        }}
        mask={cpf_mask}
        placeholderChar={'\u2000'}
        showMask={false}
      />
    );
  };

  validate = value => {
    let errorMessage;
    if (value === undefined || value === "") {
      return errorMessage;
    }
    if (!this.validateCpf(value)) {
      errorMessage = 'Número de CPF inválido';
    }
    return errorMessage;
  };

  validateCpf = cpf => {
    cpf = cpf.replace(/\D/g, "");
    let numbers, digits, sum, i, result, equalDigits;
    equalDigits = 1;
    if (cpf.length < 11) return false;
    for (i = 0; i < cpf.length - 1; i++)
      if (cpf.charAt(i) !== cpf.charAt(i + 1)) {
        equalDigits = 0;
        break;
      }
    if (!equalDigits) {
      numbers = cpf.substring(0, 9);
      digits = cpf.substring(9);
      sum = 0;
      for (i = 10; i > 1; i--) sum += numbers.charAt(10 - i) * i;
      result = sum % 11 < 2 ? 0 : 11 - (sum % 11);
      if (result !== Number(digits.charAt(0))) {
        return false;
      }
      numbers = cpf.substring(0, 10);
      sum = 0;
      for (i = 11; i > 1; i--) sum += numbers.charAt(11 - i) * i;
      result = sum % 11 < 2 ? 0 : 11 - (sum % 11);
      if (result !== Number(digits.charAt(1))) {
        return false;
      }
      return true;
    } else {
      return false;
    }
  };

  CustomInputComponent = ({ field, ...props }) => {
    const { disabled, label } = props;
    return <TextField {...field} disabled={disabled}
      autoComplete='off' variant="outlined" margin="dense" label={label}
      InputProps={{
        inputComponent: this.TextMaskCustom,
      }} />
  };

  render() {
    const { classes, name, label, disabled = false, xs = 12, sm = 6, margin = 'normal' } = this.props;
    return (
      <Grid item xs={xs} sm={sm} className={classes.item}>
        <FormControl margin={margin} fullWidth className={classes.root}>
          <Field name={name} disabled={disabled} component={this.CustomInputComponent}
            autoComplete='off' validate={this.validate} label={label} />
          <ErrorMessage
            name={name}
            className={classes.errorMessage}
            component={CustomErrorMessageComponent}
          />
        </FormControl>
      </Grid>
    );
  }
}

CustomBRCPFInput.propTypes = {
  name: PropTypes.string.isRequired,
  label: PropTypes.string.isRequired,
};

export default withStyles(styles)(CustomBRCPFInput);