import React from 'react';

import PropTypes from 'prop-types';
import { Field, ErrorMessage } from 'formik';
import MaskedInput from 'react-text-mask';

import Grid from '@material-ui/core/Grid';
import FormControl from '@material-ui/core/FormControl';
import TextField from '@material-ui/core/TextField';
import withStyles from '@material-ui/core/styles/withStyles';
import CustomErrorMessageComponent from './CustomErrorMessageComponent';
import { errorMessageClass, inputClass } from '../layout/theme';

const styles = theme => ({
  errorMessage: errorMessageClass,
  root: inputClass.root,
  item: inputClass.gridItem,
});

class CustomInputMask extends React.Component {

  /**
   * Usage:
   * https://github.com/text-mask/text-mask
   */

  TextMaskCustom = (props) => {
    const { inputRef, ...other } = props;
    return (
      <MaskedInput
        {...other}
        ref={ref => {
          inputRef(ref ? ref.inputElement : null);
        }}
        mask={this.props.mask}
        placeholderChar={'\u2000'}
        showMask={false}
      />
    );
  }

  CustomInputComponent = ({ field, ...props }) => {
    const { disabled, label } = props;
    return <TextField {...field} disabled={disabled}
      autoComplete='off' label={label} variant="outlined" margin="dense"
      InputProps={{
        inputComponent: this.TextMaskCustom,
      }} />
  };

  render() {
    const { classes, name, label, disabled = false, xs = 12, sm = 6, margin = 'dense', style = {} } = this.props;
    return (
      <Grid item xs={xs} sm={sm} style={style} className={classes.item}>
        <FormControl margin={margin} fullWidth className={classes.root}>
          <Field name={name} disabled={disabled} component={this.CustomInputComponent}
            autoComplete='off' label={label} />
          <ErrorMessage
            name={name}
            className={classes.errorMessage}
            component={CustomErrorMessageComponent}
          />
        </FormControl>
      </Grid>
    );
  }
}

CustomInputMask.propTypes = {
  name: PropTypes.string.isRequired,
  label: PropTypes.string.isRequired,
};

export default withStyles(styles)(CustomInputMask);
