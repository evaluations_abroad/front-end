import React, { Fragment } from 'react';

import PropTypes from 'prop-types';
import { Field, ErrorMessage } from 'formik';

import Grid from '@material-ui/core/Grid';
import FormControl from '@material-ui/core/FormControl';

import TextField from '@material-ui/core/TextField';
import Chip from '@material-ui/core/Chip';
import InputAdornment from '@material-ui/core/InputAdornment';
import IconButton from '@material-ui/core/IconButton';
import AddCircle from '@material-ui/icons/AddCircle';
import { withStyles } from '@material-ui/core/styles';
import CustomErrorMessageComponent from './CustomErrorMessageComponent';
import { errorMessageClass } from '../layout/theme';

const styles = theme => ({
  errorMessage: errorMessageClass,
  chip: {
    margin: `${2}px ${4}px`
  },
  iconButton: {
    padding: `${0}px`
  },
  valueContainer: {
    flexWrap: 'wrap',
    flex: 1,
    display: 'flex',
    alignItems: 'center'
  },
  placeholder: {
    position: 'absolute',
    left: 2,
    fontSize: 16
  }
});
class CustomTagsInput extends React.Component {
  constructor(props) {
    super(props);

    const { initialValues } = props.highLevelProps;
    const values = initialValues[props.name];
    this.state = {
      value: '',
      values:
        values && Array.isArray(values)
          ? values.map((it, idx) => ({ id: idx, value: it }))
          : ''
    };
  }

  componentWillReceiveProps = (nextProps) => {
    const { name, removeChips, highLevelProps } = this.props;
    if (nextProps.removeChips !== removeChips) {
      this.setState({ value: '', values: [] });
      highLevelProps.setFieldValue(name, []);
    }
  }

  add = () => {
    const { values, value } = this.state;
    if (value && value !== '') {
      let updatedValues = [...values];
      if (!values.map(it => it.value.toString()).includes(value)) {
        updatedValues.push({ id: values.length + 1, value: this.props.type === 'number' ? Number(value) : value });
      }
      this.setState({
        values: updatedValues,
        value: ''
      });
      this.props.highLevelProps.setFieldValue(
        this.props.name,
        updatedValues.map(it => it.value)
      );
    }
  };

  remove = id => {
    const { values } = this.state;
    const updatedValues = values.filter(it => it.id !== id);
    this.setState({ values: updatedValues });
    this.props.highLevelProps.setFieldValue(
      this.props.name,
      updatedValues.length > 0 ? updatedValues.map(it => it.value) : undefined
    );
  };

  inputComponent({ inputRef, ...props }) {
    if (props.chips.length > 0) {
      return (
        <div className={props.classes.valueContainer}>
          {props.chips}
        </div>
      );
    }
  }

  CustomInputComponent = ({ field, ...props }) => {
    const { value } = this.state;
    const { classes, label, disabled = false, type = 'text', adornmentLabel = '', inputProps = {} } = this.props;
    return (
      <TextField
        disabled={disabled}
        value={value}
        type={type}
        label={label}
        onKeyPress={e => {
          const { value } = e.target;
          if (e.key === 'Enter') {
            e.preventDefault();
            if (type === 'number') {
              const { max, min } = inputProps;
              if ((max >= 0 && Number(value) > max) ||
                (min >= 0 && Number(value) < min)
              ) {
                this.setState({ value: '' });
                return;
              }
            }
            this.add();
          }
        }}
        onChange={e => this.setState({ value: e.target.value })}
        inputProps={inputProps}
        // eslint-disable-next-line
        InputProps={{
          endAdornment: (
            <Fragment>
              <InputAdornment position='start'>{adornmentLabel}</InputAdornment>
              <IconButton className={classes.iconButton} onClick={this.add}>
                <AddCircle />
              </IconButton>
            </Fragment>
          )
        }}
      />
    );
  };

  render() {
    const { values } = this.state;
    const disabled = { disabled } = this.props;//eslint-disable-line
    const { classes, name, xs = 12, sm = 6, margin = 'normal' } = this.props;
    const chips = values.map((it, key) =>
      <Chip
        key={key}
        tabIndex={-1}
        label={it.value}
        className={classes.chip}
        onDelete={() => this.remove(it.id)}
      />
    );

    return (
      <Grid item xs={xs} sm={sm} classes={classes}>
        <FormControl margin={margin} fullWidth>
          <Field
            name={name}
            disabled={disabled}
            component={this.CustomInputComponent}
          />
          <div>
            {chips}
          </div>
          <ErrorMessage
            name={name}
            className={classes.errorMessage}
            component={CustomErrorMessageComponent}
          />
        </FormControl>
      </Grid>
    );
  }
}

CustomTagsInput.propTypes = {
  name: PropTypes.string.isRequired,
  label: PropTypes.string.isRequired,
  removeChips: PropTypes.bool
};

export default withStyles(styles)(CustomTagsInput);