import React from 'react';

import PropTypes from 'prop-types';
import { Field, ErrorMessage } from 'formik';

import Grid from '@material-ui/core/Grid';
import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';
import withStyles from '@material-ui/core/styles/withStyles';

import CustomCloakedInputComponent from './CustomCloakedInputComponent';
import CustomErrorMessageComponent from './CustomErrorMessageComponent';
import { errorMessageClass } from '../layout/theme';

const styles = theme => ({
  errorMessage: errorMessageClass,
  item: {
    padding: 5
  }
});

class CustomCloakedInput extends React.Component {
  render() {
    const { classes, name, label, disabled, xs = 12, sm = 6, margin = 'normal' } = this.props;
    return (
      <Grid item xs={xs} sm={sm}>
        <FormControl margin={margin} fullWidth>
          <InputLabel htmlFor={name}>{label}</InputLabel>
          <Field name={name} disabled={disabled}
            component={CustomCloakedInputComponent}
            cloakerFn={this.props.cloakerFn}
            uncloakerFn={this.props.uncloakerFn}
            highLevelProps={this.props.highLevelProps}
          />
          <ErrorMessage
            name={name}
            className={classes.errorMessage}
            component={CustomErrorMessageComponent}
          />
        </FormControl>
      </Grid>
    );
  }
}

CustomCloakedInput.propTypes = {
  name: PropTypes.string.isRequired,
  label: PropTypes.string.isRequired,
  cloakerFn: PropTypes.func.isRequired,
  uncloakerFn: PropTypes.func.isRequired
};

export default withStyles(styles)(CustomCloakedInput);
