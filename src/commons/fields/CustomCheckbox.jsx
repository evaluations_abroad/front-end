import React from 'react';

import PropTypes from 'prop-types';
import { Field, connect } from 'formik';

import Grid from '@material-ui/core/Grid';
import FormControl from '@material-ui/core/FormControl';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import withStyles from '@material-ui/core/styles/withStyles';

const styles = theme => ({});

class CustomCheckbox extends React.Component {

  handleChange = (e) => {
    const { name, onChange, formik } = this.props;
    formik.setFieldValue(name, e.target.checked ? 'true' : undefined);
    if (onChange) {
      onChange(e);
    }
  };

  CustomCheckboxComponent = ({ field, ...props }) => {

    const { disabled, enabled, label, labelPlacement } = props;
    return <FormControlLabel
      control={<Checkbox color="primary" {...field} disabled={disabled}
        onChange={this.handleChange} checked={enabled} />}
      label={label}
      labelPlacement={labelPlacement}
    />
  };

  render() {
    const { classes, name, disabled = false, onChange, xs = 12, sm = 6, label,
      margin = 'normal', labelPlacement = "end" } = this.props;
    return (
      <Grid item xs={xs} sm={sm} classes={classes} >
        <FormControl margin={margin}>
          <Field name={name} component={this.CustomCheckboxComponent}
            disabled={disabled} onChange={onChange} label={label} labelPlacement={labelPlacement} />
        </FormControl>
      </Grid>
    );
  }
}

CustomCheckbox.propTypes = {
  name: PropTypes.string.isRequired,
  label: PropTypes.string.isRequired,
};

export default withStyles(styles)(connect(CustomCheckbox));
