import React from 'react';

import PropTypes from 'prop-types';
import { Field, ErrorMessage } from 'formik';
import { connect } from 'formik';
import Grid from '@material-ui/core/Grid';
import FormControl from '@material-ui/core/FormControl';
import withStyles from '@material-ui/core/styles/withStyles';

import CustomCloakedInputComponent from './CustomCloakedInputComponent';
import CustomErrorMessageComponent from './CustomErrorMessageComponent';
import { errorMessageClass, inputClass } from '../layout/theme';
import { monetaryCloaker } from '../../utils/cloakers';

const styles = theme => ({
  errorMessage: errorMessageClass,
  root: inputClass.root,
  item: inputClass.gridItem,
});

const cloakerFn = value => monetaryCloaker(value, ',', 2);
const uncloakerFn = value => Number.parseFloat(value.toString().replace(',', '.'));

class CustomBRLInput extends React.Component {
  render() {
    const { classes, name, label, disabled, xs = 12, sm = 6, margin = 'dense' } = this.props;
    return (
      <Grid item xs={xs} sm={sm} className={classes.item}>
        <FormControl margin={margin} fullWidth className={classes.root}>
          <Field name={name}
            disabled={disabled}
            component={CustomCloakedInputComponent}
            cloakerFn={cloakerFn}
            uncloakerFn={uncloakerFn}
            highLevelProps={this.props.formik}
            label={label}
          />
          <ErrorMessage
            name={name}
            className={classes.errorMessage}
            component={CustomErrorMessageComponent}
          />
        </FormControl>
      </Grid>
    );
  }
}

CustomBRLInput.propTypes = {
  name: PropTypes.string.isRequired,
  label: PropTypes.string.isRequired
};

export default withStyles(styles)(connect(CustomBRLInput));
