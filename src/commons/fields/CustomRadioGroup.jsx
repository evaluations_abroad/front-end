import React from 'react';

import PropTypes from 'prop-types';
import { Field, ErrorMessage } from 'formik';

import Grid from '@material-ui/core/Grid';
import FormControl from '@material-ui/core/FormControl';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import withStyles from '@material-ui/core/styles/withStyles';
import CustomErrorMessageComponent from './CustomErrorMessageComponent';
import { errorMessageClass } from '../layout/theme';

const styles = theme => ({
  errorMessage: errorMessageClass
});

class CustomRadioGoup extends React.Component {
  constructor(props) {
    super(props);

    this.handleChange = this.handleChange.bind(this);
    this.setFieldValue = this.setFieldValue.bind(this);
  }

  handleChange(event) {
    const { onChange, highLevelProps } = this.props;

    if (onChange) {
      onChange(event);
    }

    highLevelProps.setFieldValue(this.props.name, event.target.value);
  }

  setFieldValue(value) {
    this.props.highLevelProps.setFieldValue(this.props.name, value);
  }

  CustomInputComponent = ({ field, ...props }) => {
    const { options, disabled, row } = this.props;
    return (
      <RadioGroup
        {...field}
        onChange={this.handleChange}
        row={row}
      >
        {options.map((it, i) => <FormControlLabel value={it.value} control={<Radio />} label={it.label} disabled={disabled} />)}
      </RadioGroup>
    );
  };

  render() {
    const { classes, name, label, xs = 12, sm = 6, margin = 'normal' } = this.props;
    return (
      <Grid item xs={xs} sm={sm} alignContent={'center'}>
        <FormControl margin={margin} fullWidth>
          {/* <InputLabel htmlFor={name}>{label}</InputLabel> */}
          <Field name={name} component={this.CustomInputComponent} />
          <ErrorMessage
            name={name}
            className={classes.errorMessage}
            component={CustomErrorMessageComponent}
          />
        </FormControl>
      </Grid>
    );
  }
}

CustomRadioGoup.propTypes = {
  name: PropTypes.string.isRequired,
  label: PropTypes.string.isRequired,
};

export default withStyles(styles)(CustomRadioGoup);
