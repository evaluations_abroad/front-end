import React from 'react';
import PropTypes from 'prop-types';
import withStyles from '@material-ui/core/styles/withStyles';
import { monetaryCloaker } from '../../utils/cloakers';
import TextField from '@material-ui/core/TextField';
import InputAdornment from '@material-ui/core/InputAdornment';
import { inputClass } from '../layout/theme';

const styles = theme => ({
  root: inputClass.root,
  item: inputClass.gridItem,
});

const cloakerFn = value => monetaryCloaker(value, ',', 2);
const uncloakerFn = value => Number.parseFloat(value.toString().replace(',', '.'));

class SimpleBRLInput extends React.Component {

  handleChange = (e) => {
    const { onChange } = this.props;
    if (onChange) { onChange(uncloakerFn(cloakerFn(e.target.value))); }
  };

  render() {
    const { label, disabled, value } = this.props;
    return (
      <TextField
        label={label}
        inputProps={{
          style: { textAlign: 'right' }
        }}
        InputProps={{
          startAdornment: (
            <InputAdornment>R$</InputAdornment>
          )
        }}
        value={cloakerFn(value)}
        disabled={disabled}
        onChange={this.handleChange}
        variant="outlined"
        margin="dense"
      />
    );
  }
}

SimpleBRLInput.propTypes = {
  name: PropTypes.string.isRequired,
  label: PropTypes.string.isRequired
};

export default withStyles(styles)(SimpleBRLInput);
