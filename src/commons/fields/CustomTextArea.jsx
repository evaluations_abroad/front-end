import React from 'react';

import PropTypes from 'prop-types';
import { Field, ErrorMessage } from 'formik';

import Grid from '@material-ui/core/Grid';
import FormControl from '@material-ui/core/FormControl';
import Input from '@material-ui/core/Input';
import InputLabel from '@material-ui/core/InputLabel';
import withStyles from '@material-ui/core/styles/withStyles';
import CustomErrorMessageComponent from './CustomErrorMessageComponent';
import { errorMessageClass } from '../layout/theme';

const styles = theme => ({
  errorMessage: errorMessageClass
});
class CustomTextArea extends React.Component {
  CustomInputComponent = ({ field, ...props }) => {
    return <Input {...field} multiline={true} />
  };

  render() {
    const { classes, name, label, xs = 12, sm = 6, margin = 'normal' } = this.props;
    return (
      <Grid item xs={xs} sm={sm}>
        <FormControl margin={margin} fullWidth>
          <InputLabel htmlFor={name}>{label}</InputLabel>
          <Field name={name} component={this.CustomInputComponent} autoComplete='off' />
          <ErrorMessage
            name={name}
            className={classes.errorMessage}
            component={CustomErrorMessageComponent}
          />
        </FormControl>
      </Grid>
    );
  }
}

CustomTextArea.propTypes = {
  name: PropTypes.string.isRequired,
  label: PropTypes.string.isRequired,
};

export default withStyles(styles)(CustomTextArea);