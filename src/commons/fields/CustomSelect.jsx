import React from 'react';

import PropTypes from 'prop-types';
import { Field, ErrorMessage } from 'formik';
import OutlinedInput from '@material-ui/core/OutlinedInput';
import Grid from '@material-ui/core/Grid';
import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import withStyles from '@material-ui/core/styles/withStyles';
import CustomErrorMessageComponent from './CustomErrorMessageComponent';
import { errorMessageClass, inputClass } from '../layout/theme';

const styles = theme => ({
  select: {
    marginBottom: '4px'
  },
  errorMessage: errorMessageClass,
  root: {
    ...inputClass.root,
    marginTop: '13px'
  },
  item: {
    ...inputClass.gridItem,
    paddingBottom: '5px!important'
  },
});
class CustomSelect extends React.Component {
  constructor(props) {
    super(props);

    this.handleChange = this.handleChange.bind(this);
    this.setFieldValue = this.setFieldValue.bind(this);
  }

  handleChange(event) {
    const { onChange, highLevelProps } = this.props;

    if (onChange) {
      onChange(event);
    }

    highLevelProps.setFieldValue(this.props.name, event.target.value);
  }

  setFieldValue(value) {
    this.props.highLevelProps.setFieldValue(this.props.name, value);
  }

  CustomInputComponent = ({ field, ...props }) => {
    const { options, disabled, multiple, labelWidth = 70 } = this.props;
    return (
      <Select {...field} onChange={this.handleChange} disabled={disabled} multiple={multiple}
        input={
          <OutlinedInput {...field} labelWidth={labelWidth} margin="dense" />
        }>>
        {options.map((it, i) => <MenuItem key={i} value={it.value}>{it.label}</MenuItem>)}
      </Select>
    );
  };

  render() {
    const { classes, name, label, xs = 12, sm = 6, margin = 'dense', labelWidth } = this.props;
    return (
      <Grid item xs={xs} sm={sm} className={classes.item}>
        <FormControl margin={margin} fullWidth variant="outlined" className={classes.root}>
          <InputLabel htmlFor={name}>{label}</InputLabel>
          <Field name={name} component={this.CustomInputComponent} labelWidth={labelWidth} />
          <ErrorMessage
            name={name}
            className={classes.errorMessage}
            component={CustomErrorMessageComponent}
          />
        </FormControl>
      </Grid>
    );
  }
}

CustomSelect.propTypes = {
  name: PropTypes.string.isRequired,
  label: PropTypes.string.isRequired,
};

export default withStyles(styles)(CustomSelect);
