import React from 'react';
import { connect } from 'formik';
import PropTypes from 'prop-types';
import { Field, ErrorMessage } from 'formik';
// import classNames from 'classnames';
import Grid from '@material-ui/core/Grid';
import FormControl from '@material-ui/core/FormControl';
import TextField from '@material-ui/core/TextField';
import withStyles from '@material-ui/core/styles/withStyles';
import CustomErrorMessageComponent from './CustomErrorMessageComponent';
import { errorMessageClass, inputClass } from '../layout/theme';

const styles = theme => ({
  errorMessage: errorMessageClass,
  root: inputClass.root,
  item: inputClass.gridItem,
});
class CustomInput extends React.Component {

  handleChange(e) {
    const { formik, onChange, name } = this.props;
    formik.setFieldValue(name, e.target.value);
    if (onChange) {
      onChange(e);
    }
  }

  CustomInputComponent = ({ field, ...props }) => {
    const { disabled, maxLength, label, type = "text" } = props;
    return <TextField {...field} disabled={disabled} autoComplete='off'
      label={label} inputProps={{ maxLength: maxLength }}
      variant="outlined" type={type}
      margin="dense" onChange={this.handleChange.bind(this)} />
  };

  render() {
    const { classes, name, label, disabled = false, xs = 12, sm = 6, margin = 'dense',
      style = {}, maxLength = undefined, type } = this.props;
    return (
      <Grid item xs={xs} sm={sm} style={style} className={classes.item}>
        <FormControl margin={margin} fullWidth className={classes.root}>
          <Field name={name} disabled={disabled} maxLength={maxLength}
            label={label} type={type}
            component={this.CustomInputComponent} />
          <ErrorMessage
            name={name}
            className={classes.errorMessage}
            component={CustomErrorMessageComponent}
          />
        </FormControl>
      </Grid>
    );
  }
}

CustomInput.propTypes = {
  name: PropTypes.string.isRequired,
  label: PropTypes.string.isRequired,
};

export default withStyles(styles)(connect(CustomInput));
