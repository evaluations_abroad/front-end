import React from 'react';

import PropTypes from 'prop-types';
import { Field, ErrorMessage } from 'formik';

import Grid from '@material-ui/core/Grid';
import FormControl from '@material-ui/core/FormControl';
import Typography from '@material-ui/core/Typography';

import TextField from '@material-ui/core/TextField';
import Chip from '@material-ui/core/Chip';
import AttachFile from '@material-ui/icons/AttachFile';
import IconButton from '@material-ui/core/IconButton';
import CloudUpload from '@material-ui/icons/CloudUpload';

import withStyles from '@material-ui/core/styles/withStyles';
import CustomErrorMessageComponent from './CustomErrorMessageComponent';
import { errorMessageClass } from '../layout/theme';

const styles = theme => ({
  errorMessage: errorMessageClass,
  chip: {
    margin: `${2}px ${4}px`,
  },
  iconButton: {
    padding: `${8}px`,
  },
  valueContainer: {
    flexWrap: "wrap",
    flex: 1,
    display: "flex",
    alignItems: "center",
  },
  placeholder: {
    position: 'absolute',
    left: 2,
    fontSize: 16,
  },
  hiddenInputFile: {
    display: 'none'
  }
});
class CustomInputFile extends React.Component {

  constructor(props) {
    super(props);
    this.fileUpload = React.createRef();
    this.state = {
      value: [],
      key: new Date().getTime(),
      modelFiles: props.modelFiles || []
    };
  }

  remove = (id) => {
    const { key, modelFiles } = this.state;
    const updatedModelFiles = modelFiles.filter(item => item.id !== id);
    const updatedFiles = updatedModelFiles.map(item => item.file);
    const hasFile = (updatedFiles.length > 0);
    this.setState({ modelFiles: updatedModelFiles });
    this.props.highLevelProps.setFieldValue(this.props.name, hasFile ? updatedFiles : "");
    const inputFile = document.getElementById(`file_${key}`);
    if (inputFile.files.length === 1) {
      inputFile.value = ''
    }
  };

  showFileUpload = () => {
    if (this.fileUpload) {
      this.fileUpload.current.click();
    }
  }

  handleChange = (e) => {
    e.preventDefault();
    let reader = new FileReader();
    let files = e.target.files;
    if (files) {
      reader.onloadend = () => {
        this.setState({
          files: files
        });
      };
      const hasFile = (e.target && [...files].length > 0);
      this.props.highLevelProps.setFieldValue(this.props.name, hasFile ? files : '');
      this.setState({
        modelFiles: hasFile ?
          [...files].map((file, index) => ({ id: index, file: file })) : [],
      });
    }
  }

  inputComponent({ inputRef, ...props }) {
    if (props.chips.length > 0) {
      return <div className={props.classes.valueContainer} >{props.chips}</div>;
    } else {
      return <div className={props.classes.valueContainer}>
        <Typography
          color="textSecondary"
          className={props.classes.placeholder}
        >
          {props.placeholder}
        </Typography>
      </div>
    }
  };

  CustomInputComponent = ({ field, ...props }) => {
    const { classes, name, accept = "*", multiple = false } = this.props;
    return <input
      className={classes.hiddenInputFile}
      name={name}
      accept={accept}
      id={`file_${this.state.key}`}
      multiple={multiple}
      type="file"
      style={{ display: "text" }}
      onChange={this.handleChange}
      ref={this.fileUpload}
    />
  };

  render() {
    const { modelFiles } = this.state;
    const { classes, name, label, xs = 12, sm = 6, margin = 'normal', props = {} } = this.props;

    const chips = modelFiles.map((item, key) =>
      <Chip
        key={key}
        tabIndex={-1}
        label={item.file.name}
        className={classes.chip}
        icon={<AttachFile />}
        onDelete={() => this.remove(item.id)}
      />
    );

    return (
      <Grid item xs={xs} sm={sm}>
        <FormControl margin={margin} fullWidth>
          <TextField
            readOnly={true}
            placeholder={label}
            InputLabelProps={{
              shrink: true,
            }}
            InputProps={{
              inputComponent: this.inputComponent,
              inputProps: {
                inputRef: props.innerRef,
                children: props.children,
                classes: classes,
                ...props.innerProps,
                chips: chips,
              },
              endAdornment: <IconButton className={classes.iconButton} onClick={this.showFileUpload} >
                <CloudUpload />
              </IconButton>
            }}
          />
          <Field name={name} component={this.CustomInputComponent} />
          <ErrorMessage
            name={name}
            className={classes.errorMessage}
            component={CustomErrorMessageComponent}
          />
        </FormControl>
      </Grid>
    );
  }
}

CustomInputFile.propTypes = {
  name: PropTypes.string.isRequired,
  label: PropTypes.string.isRequired,
};

export default withStyles(styles)(CustomInputFile);