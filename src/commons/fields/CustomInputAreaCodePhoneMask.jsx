import React from 'react';

import PropTypes from 'prop-types';
import { Field, ErrorMessage } from 'formik';
import MaskedInput from 'react-text-mask';

import Grid from '@material-ui/core/Grid';
import FormControl from '@material-ui/core/FormControl';
import TextField from '@material-ui/core/TextField';
import withStyles from '@material-ui/core/styles/withStyles';
import CustomErrorMessageComponent from './CustomErrorMessageComponent';

import { brazilian_area_code_cellphone_mask, brazilian_area_code_cellphone_mask_alternative } from '../../utils/masks';
import { errorMessageClass, inputClass } from '../layout/theme';

const styles = theme => ({
  errorMessage: errorMessageClass,
  root: inputClass.root,
  item: inputClass.gridItem,
});

class CustomInputAreaCodePhoneMask extends React.Component {

  state = {
    mask: brazilian_area_code_cellphone_mask
  }

  /**
   * Usage:
   * https://github.com/text-mask/text-mask
   */

  TextMaskCustom = (props) => {
    const { inputRef, ...other } = props;
    return (
      <MaskedInput
        {...other}
        ref={ref => {
          inputRef(ref ? ref.inputElement : null);
        }}
        mask={this.updateMask(props.value)}
        placeholderChar={'\u2000'}
        showMask={false}
      />
    );
  };

  handleChange = event => {
    const value = event.target.value.trim();
    this.setState({ mask: this.updateMask(value) });
    this.props.highLevelProps.setFieldValue(this.props.name, value);
  };

  updateMask = (value) => {
    const number = value.replace(/[^0-9]/g, "");
    let mask = brazilian_area_code_cellphone_mask;
    for (let i = 0; i < number.length; i++) {
      const digit = number.slice(i, i + 1);
      if (i === 2 && digit > 5) {
        mask = brazilian_area_code_cellphone_mask_alternative;
      }
    }
    return mask;
  };

  CustomInputComponent = ({ field, ...props }) => {
    const { disabled, label } = props;
    return <TextField {...field} disabled={disabled}
      autoComplete='off' onChange={this.handleChange} label={label}
      variant="outlined" margin="dense"
      InputProps={{
        inputComponent: this.TextMaskCustom,
      }} />
  };

  render() {
    const { classes, name, label, disabled = false, xs = 12, sm = 6, margin = 'dense' } = this.props;
    return (
      <Grid item xs={xs} sm={sm} className={classes.item}>
        <FormControl margin={margin} fullWidth className={classes.root}>
          <Field name={name} disabled={disabled} component={this.CustomInputComponent}
            autoComplete='off' label={label} />
          <ErrorMessage
            name={name}
            className={classes.errorMessage}
            component={CustomErrorMessageComponent}
          />
        </FormControl>
      </Grid>
    );
  }
}

CustomInputAreaCodePhoneMask.propTypes = {
  name: PropTypes.string.isRequired,
  label: PropTypes.string.isRequired,
};

export default withStyles(styles)(CustomInputAreaCodePhoneMask);