import React from 'react';

import PropTypes from 'prop-types';
import { Field, ErrorMessage } from 'formik';

import Grid from '@material-ui/core/Grid';
import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import withStyles from '@material-ui/core/styles/withStyles';
import CustomErrorMessageComponent from './CustomErrorMessageComponent';
import OutlinedInput from '@material-ui/core/OutlinedInput';
import { errorMessageClass, inputClass } from '../layout/theme';

const styles = theme => ({
  select: {
    marginBottom: '4px'
  },
  errorMessage: errorMessageClass,
  root: {
    ...inputClass.root,
    marginTop: '13px'
  },
  item: {
    ...inputClass.gridItem,
    paddingBottom: '5px!important'
  },
});

class CustomBRStatesSelect extends React.Component {
  constructor(props) {
    super(props);

    this.handleChange = this.handleChange.bind(this);
    this.setFieldValue = this.setFieldValue.bind(this);
  }

  handleChange(event) {
    const { onChange, highLevelProps } = this.props;

    if (onChange) {
      onChange(event);
    }

    highLevelProps.setFieldValue(this.props.name, event.target.value);
  }

  setFieldValue(value) {
    this.props.highLevelProps.setFieldValue(this.props.name, value);
  }

  CustomInputComponent = ({ field, ...props }) => {
    const { disabled, multiple, classes, label } = this.props;
    return (
      <Select {...field} onChange={this.handleChange} disabled={disabled} multiple={multiple}
        className={classes.select}
        label={label}
        input={
          <OutlinedInput {...field} labelWidth={20} margin="dense" />
        }>
        <MenuItem value={"AC"}>Acre</MenuItem>
        <MenuItem value={"AL"}>Alagoas</MenuItem>
        <MenuItem value={"AP"}>Amapá</MenuItem>
        <MenuItem value={"AM"}>Amazonas</MenuItem>
        <MenuItem value={"BA"}>Bahia</MenuItem>
        <MenuItem value={"CE"}>Ceará</MenuItem>
        <MenuItem value={"DF"}>Distrito Federal</MenuItem>
        <MenuItem value={"ES"}>Espírito Santo</MenuItem>
        <MenuItem value={"GO"}>Goiás</MenuItem>
        <MenuItem value={"MA"}>Maranhão</MenuItem>
        <MenuItem value={"MT"}>Mato Grosso</MenuItem>
        <MenuItem value={"MS"}>Mato Grosso do Sul</MenuItem>
        <MenuItem value={"MG"}>Minas Gerais</MenuItem>
        <MenuItem value={"PA"}>Pará</MenuItem>
        <MenuItem value={"PB"}>Paraíba</MenuItem>
        <MenuItem value={"PR"}>Paraná</MenuItem>
        <MenuItem value={"PE"}>Pernambuco</MenuItem>
        <MenuItem value={"PI"}>Piauí</MenuItem>
        <MenuItem value={"RJ"}>Rio de Janeiro</MenuItem>
        <MenuItem value={"RN"}>Rio Grande do Norte</MenuItem>
        <MenuItem value={"RS"}>Rio Grande do Sul</MenuItem>
        <MenuItem value={"RO"}>Rondônia</MenuItem>
        <MenuItem value={"RR"}>Roraima</MenuItem>
        <MenuItem value={"SC"}>Santa Catarina</MenuItem>
        <MenuItem value={"SP"}>São Paulo</MenuItem>
        <MenuItem value={"SE"}>Sergipe</MenuItem>
        <MenuItem value={"TO"}>Tocantins</MenuItem>
      </Select>
    );
  };

  render() {
    const { classes, name, label, xs = 12, sm = 6, margin = 'dense' } = this.props;
    return (
      <Grid item xs={xs} sm={sm} className={classes.item}>
        <FormControl margin={margin} fullWidth variant="outlined" className={classes.root}>
          <InputLabel htmlFor={name}>{label}</InputLabel>
          <Field name={name} component={this.CustomInputComponent} classes={classes} />
          <ErrorMessage
            name={name}
            className={classes.errorMessage}
            component={CustomErrorMessageComponent}
          />
        </FormControl>
      </Grid>
    );
  }
}

CustomBRStatesSelect.propTypes = {
  name: PropTypes.string.isRequired,
  label: PropTypes.string.isRequired,
};

export default withStyles(styles)(CustomBRStatesSelect);
