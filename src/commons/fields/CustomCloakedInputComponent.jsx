
import React from 'react';
import TextField from '@material-ui/core/TextField';
import InputAdornment from '@material-ui/core/InputAdornment';

export default ({ field, ...props }) => {
  const { highLevelProps, onChange, cloakerFn, uncloakerFn, disabled, label } = props;

  const handleChange = (e) => {
    highLevelProps.setFieldValue(field.name, uncloakerFn(cloakerFn(e.target.value)));
    if (onChange) { onChange(e); }
  };

  return (
    <TextField
      {...{
        ...field,
        value: props.cloakerFn(field.value)
      }}
      inputProps={{
        style: { textAlign: 'right' }
      }}
      InputProps={{
        startAdornment: (
          <InputAdornment>R$</InputAdornment>
        )
      }}
      disabled={disabled}
      onChange={handleChange}
      variant="outlined"
      margin="dense"
      label={label}
    />
  );
};
