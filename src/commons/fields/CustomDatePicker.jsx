import React from 'react';
import PropTypes from 'prop-types';

import FormControl from '@material-ui/core/FormControl';
import Grid from '@material-ui/core/Grid';
import withStyles from '@material-ui/core/styles/withStyles';
import TextField from '@material-ui/core/TextField';
import { ErrorMessage, Field } from 'formik';
import CustomErrorMessageComponent from './CustomErrorMessageComponent';
import { errorMessageClass } from '../layout/theme';

const styles = theme => ({
  errorMessage: errorMessageClass
});
class CustomDatePicker extends React.Component {
  CustomInputComponent = ({ field, ...props }) => {
    return (
      <TextField
        {...field}
        type={this.props.type ? this.props.type : 'date'}
        label={this.props.label}
        disabled={this.props.disabled}
        InputLabelProps={{
          shrink: true,
        }}
      />
    );
  };

  render() {
    const { classes, name, xs = 12, sm = 6, margin = 'normal', disabled = false } = this.props;
    return (
      <Grid item xs={xs} sm={sm}>
        <FormControl margin={margin} fullWidth>
          <Field name={name} component={this.CustomInputComponent} disabled={disabled} autoComplete='off'/>
          <ErrorMessage
            name={name}
            className={classes.errorMessage}
            component={CustomErrorMessageComponent}
          />
        </FormControl>
      </Grid>
    );
  }
}

CustomDatePicker.propTypes = {
  name: PropTypes.string.isRequired,
  label: PropTypes.string.isRequired,
};

export default withStyles(styles)(CustomDatePicker);
