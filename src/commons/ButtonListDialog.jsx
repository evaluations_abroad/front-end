import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import { withStyles } from '@material-ui/core/styles';

const styles = theme => ({
  button: {
    padding: '8px 10px',
    width: 110,
    height: 100,
    marginRight: '10px',
    marginBottom: '10px',
    textTransform: 'capitalize'
  }
});

class ButtonListDialog extends Component {

  state = {
    open: this.props.open || false
  };

  componentWillReceiveProps(nextProps) {
    this.setState({ open: nextProps.open });
  }

  handleClose = () => {
    this.setState({ open: this.props.handleClose ? this.props.handleClose() : false });
  };

  render() {

    const { open } = this.state;

    const {
      title,
      description,
      classes,
      items,
      handleClick = () => alert('Implements handleSubmit'),
      labelProp
    } = this.props;

    return (
      <div>
        <Dialog
          open={open} maxWidth="xl"
          onClose={this.handleClose}
          aria-labelledby="form-dialog-title"
        >
          <DialogTitle id="form-dialog-title">{title}</DialogTitle>
          <DialogContent>
            <DialogContentText>
              {description}
            </DialogContentText>
            {
              items.map((item, index) => (
                <Button size="small" key={`category${index}`} classes={{ root: classes.button }} variant="contained" color="default"
                  onClick={() => handleClick(item)}>
                  {item[labelProp]}
                </Button>
              ))
            }
          </DialogContent>
        </Dialog>
      </div>
    );
  }
}

ButtonListDialog.propTypes = {
  open: PropTypes.bool,
  title: PropTypes.string,
  items: PropTypes.array,
  handleClick: PropTypes.func.isRequired,
  labelProp: PropTypes.string
};

export default withStyles(styles)(ButtonListDialog);