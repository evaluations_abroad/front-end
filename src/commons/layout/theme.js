import { createMuiTheme } from '@material-ui/core/styles';

const theme = createMuiTheme({
  typography: {
    useNextVariants: true,
    body1: {
      color: '#757575',
    },
    body2: {
      color: '#757575',
    },
    h4: {
      color: '#757575',
    },
  },
  palette: {
    primary: {
      light: '#f44336',
      main: '#3c8dbc',
      dark: '#3c8dbc',
      contrastText: '#fff',
    },
    secondary: {
      light: '#f44336',
      main: '#3c8dbc',
      dark: '#3c8dbc',
      contrastText: '#fff',
    }
  },
});

export const errorMessageClass = {
  color: '#d32f2f',
  fontFamily: '"Goudy Bookletter 1911", sans-serif',
  fontSize: 14,
  fontWeight: 'bolder'
}

export const inputClass = {
  root: {
    height: '48px',
    marginTop: '5px'
  },
  gridItem: {
    paddingLeft: '12px!important',
    paddingRight: '12px!important',
    paddingTop: '7px!important',
    paddingBotton: '12px!important'
  }
}

export default theme;