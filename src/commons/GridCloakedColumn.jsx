import React from 'react';

import TextField from '@material-ui/core/TextField';
import Input from '@material-ui/core/Input';

const renderCellBody = ({ rowData, column }, props) => {
  const {
    columnDataResolver,
    inputProps,
    error,
    cloakerFn,
    interactable
  } = props;

  const {
    value,
    inputProps: resolvedInputProps,
    error: resolvedError
  } = columnDataResolver(rowData, column);
  const cloakedValue = cloakerFn(value);

  if (!interactable) { return ( cloakedValue ); }
  return (
    <TextField
      error={resolvedError || error}
      value={cloakedValue}
      inputProps={{
        tabIndex: -1, //Prevents tabbing into another cell.
        ...{
          ...inputProps,
          ...resolvedInputProps
        }
      }}
    />
  );
};

const renderCellEditor = ({ rowData, column }, props) => {
  const {
    columnDataResolver,
    inputProps,
    cloakerFn
  } = props;

  const {
    id,
    name,
    value,
    onChange = () => console.error('onChange Fn must produced on columnDataResolver Fn!'),
    inputProps: resolvedInputProps
  } = columnDataResolver(rowData, column);
  const cloakedValue = cloakerFn(value);

  return (
    <Input
      name={name}
      id={id}
      value={cloakedValue}
      onChange={onChange()}
      {...{
        inputProps: {
          ...inputProps,
          ...resolvedInputProps
        }
      }}
    />
  );
};

const GridCloakedColumn = props => {
  const {
    columnData = {},
    cloakerFn = () => console.error('cloaker Fn must be provided!'),
    error = false,
    columnDataResolver = () => console.error('columnDataResolver Fn must be provided!'),
    inputProps = {},
    interactable = false
  } = props;

  return {
    ...columnData,
    body: (rowData, column) => renderCellBody({ rowData, column }, { columnDataResolver, inputProps, error, cloakerFn, interactable }),
    ...interactable &&
      { editor: column => renderCellEditor({ rowData: column.rowData, column }, { columnDataResolver, inputProps, error, cloakerFn }) }
  };
};

export default GridCloakedColumn;
