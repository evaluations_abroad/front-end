import React from 'react';
import Button from '@material-ui/core/Button';

const ActionColumn = props => {
  const { buttonsClass, containerClass, buttons = [] } = props;

  return (
    <div className={containerClass}>
      {buttons.map(({ label, handler, buttonClass, disabled = false }, index) => (
        <Button key={index}
          size="small"
          color="primary"
          variant="outlined"
          onClick={handler}
          disabled={disabled}
          className={buttonClass || buttonsClass}
        >
          {label}
        </Button>
      ))}
    </div>
  );
};

export default ActionColumn;
