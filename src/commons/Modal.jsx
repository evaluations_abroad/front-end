import React, { Component } from 'react';
import PropTypes from 'prop-types';

import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';

export default class Modal extends Component {

  state = {
    open: this.props.open || false
  };

  componentWillReceiveProps(nextProps) {
    this.setState({ open: nextProps.open });
  }

  handleClose = () => {
    this.setState({ open: this.props.handleClose ? this.props.handleClose() : false });
  };

  render() {

    const { open } = this.state;

    const {
      title,
      description,
      children,
      dialogProps = {},
      dialogPropsTitle = {}
    } = this.props;

    return (
      <div>
        <Dialog
          open={open}
          onClose={this.handleClose}
          aria-labelledby="form-dialog-title"
          {...dialogProps}
        >
          <DialogTitle id="form-dialog-title" {...dialogPropsTitle }>{title}</DialogTitle>
          <DialogContent>
            <DialogContentText>
              {description}
            </DialogContentText>
            {children}
          </DialogContent>
        </Dialog>
      </div>
    );
  }
}

Modal.propTypes = {
  open: PropTypes.bool,
  title: PropTypes.string,
  description: PropTypes.string,
  handleClose: PropTypes.func
};