import * as Yup from 'yup';

export const equalTo = (ref, msg) => {
  return Yup.mixed().test({
    name: 'equalTo',
    exclusive: false,
    // eslint-disable-next-line
    message: msg || '${path} must be the same as ${reference}',
    params: {
      reference: ref.path,
    },
    test: function (value) {
      return value === this.resolve(ref);
    },
  });
}

export const isFulfilledWhenMandatoryFieldsAreEmpty = (mandatoryFields, fallbackFields, fallbackFieldsResolverFunction, validationErrorMessage) => {
  return Yup.mixed().test({
    name: 'isFulfilledWhenMandatoryFieldsAreEmpty',
    exclusive: false,
    // eslint-disable-next-line
    message: validationErrorMessage || 'There was an error when validating this form',
    test: function () {
      if (mandatoryFields.filter(field =>
        (this.resolve(Yup.ref(field)))
      ).length) {
        return true;
      }

      return fallbackFieldsResolverFunction(
        fallbackFields.map(field =>
          this.resolve(Yup.ref(field))
        )
      );
    },
  });
}

export const yupCustomMessages = {
  mixed: {
    notType: 'Deve ser um ${type} válido',//eslint-disable-line
    // notType: ({ type }) => {
    //   let PTBRType = "";
    //   if (type === "number") {
    //     PTBRType = "número"
    //   }
    //   let msg = 'Deve ser um ${PTBRType} válido',//eslint-disable-line
    //   return msg;
    // },
    // required: 'Campo obrigatório'
  },
  number: {
    min: 'O valor do campo deve ser no mínimo ${min}'//eslint-disable-line
  },
  string: {
    email: 'Deve ser um email válido'
  }
}

Yup.addMethod(Yup.string, 'equalTo', equalTo);
Yup.addMethod(Yup.date, 'isFulfilledWhenMandatoryFieldsAreEmpty', isFulfilledWhenMandatoryFieldsAreEmpty);

export default Yup;