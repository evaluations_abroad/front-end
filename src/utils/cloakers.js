import { splitAt } from './array';

export const monetaryCloaker = (value, decimalsSeparator = '.', decimalPlaces = 0) => {
  if (value === undefined) {
    return "";
  }
  let fixedValue = value;
  if (typeof value === 'number') {
    fixedValue = value.toFixed(decimalPlaces);
  }
  const isNegative = fixedValue < 0;
  const cleanValue = fixedValue.toString().replace(/\D/g, '');

  const significantValue = cleanValue
    .replace(/^0+/, '') // Remove leading zeros
    .replace(/\B(?=(\d{3})+(?!\d))/g, ''); // Removing trailing zeros

  let ret = splitAt(significantValue.padStart(decimalPlaces + 1, '0'), -decimalPlaces).join(decimalsSeparator);
  return isNegative ? `-${ret}` : ret;
};
