const CHERRY = {
  code: 'CHERRY',
  image: 'cherry.png',
  name: 'cherry',
  label: 'cherry'
};
const APPLE = {
  code: 'APPLE',
  image: 'apple.png',
  name: 'apple',
  label: 'apple'
};
const BANANA = {
  code: 'BANANA',
  image: 'banana.png',
  name: 'banana',
  label: 'banana'
};
const LEMON = {
  code: 'LEMON',
  image: 'lemon.png',
  name: 'lemon',
  label: 'lemon'
};

const list = () => {
  return [
    CHERRY,
    APPLE,
    BANANA,
    LEMON
  ]
}

const getByCode = (code) => {
  return list().find(type => type.code === code);
}

const getByName = (name) => {
  return list().find(type => type.name === name);
}

export default {
  CHERRY,
  APPLE,
  BANANA,
  LEMON,
  list,
  getByCode,
  getByName
};
