import axios from 'axios';
import moment from 'moment';
import pigeon from './pigeon';
import { doLogout } from '../state/auth/authActions';
import { monetaryCloaker } from './cloakers';

//whereFilters template: {prop:{eq:'value'}} or {prop:'value'}
export function gerarObjectJsonFiltros(limit, offset, whereFilters, filters) {
  var filter = {};
  filter.limit = limit;
  filter.offset = offset;
  if (filters) {
    Object.keys(filters).forEach((key) => {
      filter[key] = filters[key];
    });
  }

  if (whereFilters) {
    var where = {};
    Object.keys(whereFilters).forEach((key) => {
      if (whereFilters[key].eq) {
        where[key] = { eq: whereFilters[key].eq };
      } else if (whereFilters[key].like) {
        where[key] = { like: '%' + whereFilters[key].like + '%' };
      } else {
        where[key] = { like: '%' + whereFilters[key] + '%' };
      }
    });
    filter.where = where;
  }
  return filter;
}

export function gerarJsonFiltros(limit, offset, whereFilters, filters) {
  return JSON.stringify(gerarObjectJsonFiltros(limit, offset, whereFilters, filters));
}

export function gerarCountFiltros(whereFilters) {
  var where = '';
  if (whereFilters && Object.keys(whereFilters).length > 0) {
    where += '?';
    Object.keys(whereFilters).forEach((key) => {
      let value = whereFilters[key].value || whereFilters[key];
      let operador = '[like]';
      let percent = '%';

      if (whereFilters[key].matchMode === 'eq') {
        operador = '';
        percent = '';
      }

      where += 'where[' + key + ']' + operador + '=' + percent + value + percent + '&';
    });
  }
  return where;
}

export function gerarJsonCount(whereFilters) {
  return whereFilters ? JSON.stringify(gerarCountFiltros(whereFilters)) : "";
}

export function setupInterceptors(store) {
  axios.interceptors.response.use(
    function (response) { return response; },
    function (error) {
      if (error.response) {
        if (error.response && error.response.status === 403) {
          store.dispatch(pigeon('not_authorized'));
        } else if (error.response && error.response.status === 401) {
          store.dispatch([
            doLogout(),
            //pigeon('not_authenticated')
          ]);
        } else {
          store.dispatch(pigeon('error'));
        }
      }
      return Promise.reject(error);
    }
  );
}

export function convertStringToDate(sDate) {
  //const template = '26/04/2013';
  let pattern = /(\d{2})\/(\d{2})\/(\d{4})/;
  return new Date(sDate.replace(pattern, '$3-$2-$1'));
}

const DECIMAL_BRL_SEPARATOR_CLOAKER = ',';

export function convertBRLStringToNumber(sNumber) {
  return Number.parseFloat(sNumber.toString().replace(DECIMAL_BRL_SEPARATOR_CLOAKER, '.'));
}

export function convertNumberToStringBRL(number) {
  return monetaryCloaker(number, DECIMAL_BRL_SEPARATOR_CLOAKER, 2)
}

export function convertDateToHourAndMinute(date) {
  let hour = date.getHours().toString().length === 2 ? date.getHours() : `0${date.getHours()}`;
  let minute = date.getMinutes().toString().length === 2 ? date.getMinutes() : `0${date.getMinutes()}`;
  return `${hour}:${minute}`
}

export function validateTokenLifetime(user) {
  const lifetime = user.ttl;
  const expireDate = moment(user.created).add(lifetime, 'seconds');
  const now = moment();
  const valid = expireDate.diff(now) > 0;
  return valid;
}
