export const chunkanizeArray = (array, chunkSize) => {
  return new Array(Math.ceil(array.length / chunkSize)).fill("")
    .map(function() { return this.splice(0, chunkSize) }, array.slice());
};

export const splitAt = (array, index) => [array.slice(0, index), array.slice(index)];
