import axios from 'axios';
import consts from '../../main/consts';
import { showErrorMessage } from '../app/appActions';
import ApiAgent, { HTTP_METHOD } from '../../main/apiAgent'
import resources from '../../main/resources';

export const types = {
  AUTH_SIGNUP: 'AUTH_SIGNUP',
  AUTH_SIGNIN: 'AUTH_SIGNIN',
  AUTH_RECOVER_ACCESS: 'AUTH_RECOVER_ACCESS',
  AUTH_ACCOUNT_UPDATED: 'AUTH_ACCOUNT_UPDATED',
  AUTH_PASSWORD_UPDATED: 'AUTH_PASSWORD_UPDATED',
  AUTH_CHANGE_PASSWORD_FORM: 'AUTH_CHANGE_PASSWORD_FORM',
  AUTH_MY_ACCOUNT_FORM: 'AUTH_MY_ACCOUNT_FORM',
  AUTH_LOGOUT: 'AUTH_LOGOUT',
  AUTH_CLEAR_STORE: 'CLEAR_STORE',
  AUTH_USERS_FETCHED: 'AUTH_USERS_FETCHED',
  AUTH_USERS_FOR_APPROVAL_FETCHED: 'AUTH_USERS_FOR_APPROVAL_FETCHED',
  AUTH_APPROVED_USER_ACCOUNT: 'AUTH_APPROVED_USER_ACCOUNT',
  AUTH_DISAPPROVED_USER_ACCOUNT: 'AUTH_DISAPPROVED_USER_ACCOUNT',
  AUTH_DELETE_USER: 'AUTH_DELETE_USER'
};

export const doSignUp = (values) => dispatch => {

  // const variables = { signUpInput: { ...values } };

  return ApiAgent.submit(
    resources.OPERATORS_RESOURCE,
    HTTP_METHOD.POST,
    { params: values });
}

export const doSignIn = async (values) => (dispatch, getState) => {
  axios
    .post(`${consts.API_URL}/user/login`, values)
    .then((resp1) => {
      let user = resp1.data.data;
      axios.defaults.headers.common['Authorization'] = `${user.token}`;
      dispatch({
        type: types.AUTH_SIGNIN,
        payload: { ...user }
      });
      return user;
    })
    .catch((e) => {
      dispatch(showErrorMessage("Wrong username or password!"));
    });
}

export const recoverPassword = async () => { };
//export const recoverPassword = async ({ email }) => dispatch => (
/*   graphql(
    `
      mutation ($email: String!) {
        recoverPassword(email: $email)
      }
    `, { email }, dispatch).then(response => {
      if (response && response.data.recoverPassword) {
        dispatch({ type: types.AUTH_RECOVER_ACCESS });
        return {
          showForm: false,
          message: `Um email foi enviado para ${email}`
        }
      } else {
        dispatch(pigeon('service_unavailable'));
        return { showForm: true };
      }
    }) */
//);

export const updateAccount = async (values) => (dispatch, getState) => {
  /* 
    const { user } = getState().auth
  
    const mutation = `
      mutation ($userAccountUpdateInput: UserAccountUpdateInput!) {
        updateAccount(data: $userAccountUpdateInput) {
          name
          email
        }
      }
    `;
  
    const variables = { userAccountUpdateInput: { ...values } }; */

  /*   return graphql(mutation, variables, dispatch).then(response => {
      if (response && response.data.updateAccount) {
        dispatch({
          type: types.AUTH_ACCOUNT_UPDATED,
          payload: { ...user, name: response.data.updateAccount.name, email: response.data.updateAccount.email }
        });
        dispatch(pigeon('update_success', 'user'));
      }
    }); */

}

export const updatePassword = async (values) => dispatch => {

  /*   const mutation = `
      mutation ($updatePasswordInput: UpdatePasswordInput!) {
        updatePassword(data: $updatePasswordInput)
      }
    `;
  
    const variables = { updatePasswordInput: { ...values } }; */

  /*   return graphql(mutation, variables, dispatch).then(response => {
      if (response && response.data.updatePassword) {
        dispatch({
          type: types.AUTH_PASSWORD_UPDATED
        });
        dispatch(pigeon('update_success', 'password'));
        return true;
      }
    }); */

}

export const getUsers = async (page = 0, filters = {}) => dispatch => {

  /*   const query = `
      query ($page: Int, $filters: UserWhereInput) {
        userList(page: $page, where: $filters) {
          total
          rows {
            id
            name
            email
            createdAt
            role
          }
        }
      }
    `;
  
    const variables = { page, filters }; */

  /*   return graphql(query, variables, dispatch).then(response => {
      if (response && response.data.userList) {
        dispatch({
          type: types.AUTH_USERS_FETCHED,
          payload: response.data.userList
        });
      }
    }); */

}

export const getUsersToApproval = async (page = 0, filters = {}) => dispatch => {

  /*   const query = `
      query ($page: Int, $filters: UserWhereInput) {
        usersToApproval(page: $page, where: $filters) {
          total
          rows {
            id
            name
            email
            createdAt
          }
        }
      }
    `;
  
    const variables = { page, filters: { ...filters } }; */

  /*   return graphql(query, variables, dispatch).then(response => {
  
      if (response && response.data.usersToApproval) {
        dispatch({
          type: types.AUTH_USERS_FOR_APPROVAL_FETCHED,
          payload: response.data.usersToApproval
        });
      }
    }); */

}

export const approveUserAccount = async (values) => (dispatch) => {

  /*   const mutation = `
      mutation ($id: ID!, $role: String!) {
        approveUserAccount(id: $id, role: $role)
      }
    `;
  
    const variables = { ...values }; */

  /*   return graphql(mutation, variables, dispatch).then(response => {
      if (response && response.data.approveUserAccount) {
        dispatch({
          type: types.AUTH_APPROVED_USER_ACCOUNT
        });
        dispatch(getUsersToApproval());
        dispatch(getUsers());
        dispatch(pigeon('success'));
      } else {
        dispatch(pigeon('error'));
      }
    }); */

}

export const disapproveUserAccount = async (id) => (dispatch) => {

  /*   const mutation = `
      mutation ($id: ID!) {
        disapproveUserAccount(id: $id)
      }
    `;
  
    const variables = { id }; */

  /*   return graphql(mutation, variables, dispatch).then(response => {
      if (response && response.data.disapproveUserAccount) {
        dispatch({
          type: types.AUTH_DISAPPROVED_USER_ACCOUNT
        });
        dispatch(getUsersToApproval());
        dispatch(getUsers());
        dispatch(pigeon('success'));
      } else {
        dispatch(pigeon('error'));
      }
    }); */

}

export const deleteUser = async (id) => (dispatch) => {
  /* 
    const mutation = `
      mutation ($id: ID!) {
        deleteUser(id: $id)
      }
    `;
  
    const variables = { id }; */
  /* 
    return graphql(mutation, variables, dispatch).then(response => {
      if (response && response.data.deleteUser) {
        dispatch({
          type: types.AUTH_DELETE_USER
        });
        dispatch(getUsers());
        dispatch(pigeon('success'));
      } else {
        dispatch(pigeon('error'));
      }
    }); */
}

export const handleMyAccountFormView = (payload) => dispatch => {
  dispatch({
    type: types.AUTH_MY_ACCOUNT_FORM,
    payload: payload
  });
}

export const handleChangePasswordFormView = (payload) => dispatch => {
  dispatch({
    type: types.AUTH_CHANGE_PASSWORD_FORM,
    payload: payload
  });
}

export const doLogout = async () => dispatch => {
  dispatch({
    type: types.AUTH_LOGOUT
  });
  dispatch({
    type: types.AUTH_CLEAR_STORE
  });
};

export const validatePasswordResetToken = async token => dispatch => { }
//export const validatePasswordResetToken = async token => dispatch => (
/*  graphql(
   `
     mutation($token: String!) {
       validateToken(token: $token)
     }
   `, { token }, dispatch).then(response => {
     if (response === null) { dispatch(pigeon('validation_failure', 'token')); return false; }
     const {
       data: {
         validateToken: {
           valid = false
         } = {}
       } = {}
     } = response;

     if (!valid) { dispatch(pigeon('validation_failure', 'token')); }
     return valid;
   }) */
//);

export const resetPassword = async ({ token, password }) => dispatch => { }
//export const resetPassword = async ({ token, password }) => dispatch => (
/* graphql(
  `
    mutation($token: String!, $password: String!) {
      resetPassword(token: $token, password: $password)
    }
  `
  , { token, password }, dispatch).then(response => {
    if (response === null) { dispatch(pigeon('error')); return null; }
    const {
      data: {
        resetPassword: {
          email = null
        } = {}
      } = {}
    } = response;

    if (email) {
      dispatch(pigeon('update_success', 'password'));
    } else {
      dispatch(pigeon('error'));
    }
    return email;
  }) */
//);
