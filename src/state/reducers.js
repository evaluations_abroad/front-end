import { combineReducers } from 'redux';

import AppReducer from "../state/app/appReducer";
import AuthReducer from "../state/auth/authReducer";
import UserReducer from '../views/user/ducks/reducer';
import CountryReducer from '../views/country/ducks/reducer';
import SlotMachineReducer from '../views/slotMachine/ducks/reducer';

const appReducer = combineReducers({
  app: AppReducer,
  auth: AuthReducer,
  user: UserReducer,
  country: CountryReducer,
  slotMachine: SlotMachineReducer
});

const rootReducer = (state, action) => {
  if (action.type === 'CLEAR_STORE') return appReducer(undefined, action);
  return appReducer(state, action);
};

export default rootReducer;
