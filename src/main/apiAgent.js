import consts from './consts';
import axios from 'axios';

const limit = (max) => `max=${max}`;

export const HTTP_METHOD = {
  GET: 'get',
  POST: 'post',
  PUT: 'put',
  DELETE: 'delete'
}

const ApiAgent = {
  submit(resource, method, options) {
    const { params, config = {} } = options;
    let path = options.path ? `/${options.path}` : "";

    const id = params.id ? params.id : "";

    if (!params.id) {
      delete params.id;
    }

    if (method === HTTP_METHOD.GET) {
      config.params = params;
      delete params.id;
      return axios[method](`${consts.API_URL}/${resource}/${id}${path}`, config);
    } else {
      return axios[method](`${consts.API_URL}/${resource}/${id}${path}`, params, config);
    }
  },

  getList(resource, type) {
    return dispatch => {
      return axios.get(`${consts.API_URL}/${resource}`, {})
        .then((response) => {
          dispatch({
            type: type,
            payload: response.data.data
          });
        });
    }
  },

  autocomplete(action, query) {
    return axios.get(`${consts.API_URL}/autocomplete/${action}?q=${query}&${limit(10)}`)
      .then(result => {
        return result;
      })
      .catch(error => {
        //toastr.error('Erro', 'Erro ao efetuar busca!');
        return Promise.reject(error);
      });
  },

  autocompleteRecover(action, id) {
    return axios.get(`${consts.API_URL}/autocomplete/${action}?id=${id}`)
      .then(result => {
        return result;
      })
      .catch(error => {
        //toastr.error('Erro', 'Erro na recuperação de dados!');
        return Promise.reject(error);
      });
  },

  atualizarMeusDados(values) {
    const id = values.id ? values.id : '';
    return axios.put(`${consts.API_URL}/meus-dados/${id}`, values)
      .then(response => {
        //toastr.success('Sucesso', response.data.message);
        return response
      })
      .catch(e => {
        // defaultErrorHandler(e.response);
      })
  },
};

export default ApiAgent;