export default {
  SIGNIN_RESOURCE: "signin",
  USER_RESOURCE: "user",
  USERS_RESOURCE: "users",
  COUNTRY_RESOURCE: "country",
  COUNTRIES_RESOURCE: "countries",
  SLOT_MACHINE_RESOURCE: "slotMachine"
};
