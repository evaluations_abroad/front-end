import React, { Fragment } from 'react';
import { BrowserRouter, Switch, Route } from 'react-router-dom';

import { SnackbarProvider } from 'notistack';
import { MuiThemeProvider } from '@material-ui/core/styles';

import AuthOrApp from '../AuthOrApp';
import theme from '../commons/layout/theme';
import SignIn from '../views/auth/SignIn';
import UserList from '../views/user/containers/UserList';
import UserForm from '../views/user/containers/UserForm';
import resources from './resources';
import CountryList from '../views/country/containers/CountryList';
import SlotMachine from '../views/slotMachine/containers/slotMachineView';

export default props => (
  <MuiThemeProvider theme={theme}>
    <BrowserRouter>
      <Switch>
        <Fragment>
          <SnackbarProvider maxSnack={3}>
            <Fragment>
              <AuthOrApp>
                <Route exact path="/" />
                <Route path={`/${resources.SIGNIN_RESOURCE}`} component={SignIn} />
                <Route path={`/${resources.USERS_RESOURCE}`} component={UserList} />
                <Route path={`/${resources.USER_RESOURCE}`} component={UserForm} />
                <Route path={`/${resources.COUNTRIES_RESOURCE}`} component={CountryList} />
                <Route path={`/${resources.SLOT_MACHINE_RESOURCE}`} component={SlotMachine} />
              </AuthOrApp>
            </Fragment>
          </SnackbarProvider>
        </Fragment>
      </Switch>
    </BrowserRouter>
  </MuiThemeProvider>
);
