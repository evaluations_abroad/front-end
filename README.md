# Yobetit Evaluation frontend#

Yobetit frontend evaluation frontend project

## Setup

Follow these steps to set up the app:

1. `npm install` – install dependencies
3. `npm start` – serve the app at [http://localhost:3000/](http://localhost:3000/) (it automatically opens the app in your default browser)

# Login

To access in the 1st time use the credentials below

email: 'admin@mail.com'
password: '123456'